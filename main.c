/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/epoll.h>
#include <unistd.h>

#include "cttop.h"
#include "ct_entry.h"
#include "ct_events.h"
#include "ct_opts.h"
#include "ct_signal.h"
#include "ct_timer.h"

#include "ui.h"

static int epollfd;

#define MAX_EVENTS 2
#define PROC_SYS_NET_NETFILTER_ACCT	"/proc/sys/net/netfilter/nf_conntrack_acct"

static void dispatch(const struct epoll_event *ev)
{
	void(*function)(void);

	function = ev->data.ptr;
	function();
}

static void nonblock(int fd)
{
	int flags = fcntl(fd, F_GETFL);
	if (flags == -1) {
		perror("fcntl F_GETFL");
		exit(1);
	}

	if (fcntl(fd, F_SETFL, flags|O_NONBLOCK)) {
		perror("fcntl O_NONBLOCK");
		exit(1);
	}
}

static int loop(void)
{
	struct epoll_event events[MAX_EVENTS];
	int nfds;

	for (;;) {
		nfds = epoll_wait(epollfd, events, ARRAY_SIZE(events), -1);
		if (nfds < 0) {
			if (errno == EINTR)
				continue;
			perror("epoll_wait");
			exit(1);
		}
		while (nfds > 0) {
			dispatch(&events[--nfds]);
		}
	}
	return 0;
}

static void wantread(int fd, void (*funct)(void))
{
	struct epoll_event ev;

	ev.data.ptr = funct;
	ev.events = EPOLLIN;

	if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev) == -1) {
		perror("epoll_ctl");
		exit(EXIT_FAILURE);
	}
}

static void init_rng(void)
{
	FILE *fp = fopen("/dev/urandom", "rb");
	unsigned int seed = time(NULL);

	if (fp) {
		if (fread(&seed, sizeof(seed), 1, fp) != 1)
			perror("fread");
		fclose(fp);
	}
	srandom(seed);
}

static bool check_nf_acct(void)
{
	FILE *fp = fopen(PROC_SYS_NET_NETFILTER_ACCT, "r");
	int onoff;

	if (!fp)
		fprintf(stderr, "%s: %s\n", PROC_SYS_NET_NETFILTER_ACCT, strerror(errno));
	else if (1 == fscanf(fp, "%d", &onoff) && onoff == 1)
		return true;
	return false;
}

int main(int argc, char *argv[])
{
	int fd;

	if (!check_nf_acct()) {
		fprintf(stderr, "Warning: %s must be set to 1 for conntrack counter support\n",
				PROC_SYS_NET_NETFILTER_ACCT);
		sleep(5);
	}

	cttop_parse_options(argc, argv);

	epollfd = epoll_create(10);
	if (epollfd < 0) {
		perror("epoll_create");
		exit(1);
	}

	init_rng();

	ct_entry_init();
	fd = ctevent_init();
	nonblock(fd);
	wantread(fd, ctevent_dispatch);
	fd = ctsignal_init();
	wantread(fd, ctsignal_dispatch);

	ui_init();

	if (!cttop_options->batch_mode)
		wantread(0, ui_dispatch);

	fd = cttimer_init();
	wantread(fd, cttimer_dispatch);
	loop();
	return 0;
}
