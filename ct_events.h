#include <stdbool.h>


void ctevent_dispatch(void);
int ctevent_init(void);

struct nf_conntrack;
bool ctevent_query(struct nf_conntrack *ct);
