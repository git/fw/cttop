/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <libnetfilter_conntrack/libnetfilter_conntrack.h>

#include "ct_entry.h"
#include "ct_opts.h"

static struct nfct_handle *nfct_handle;
static struct nfct_handle *nfct_query_h;

struct maskv32 {
	uint32_t value;
	uint32_t mask;
};

static bool maskv32_match(struct maskv32 *m, uint32_t val)
{
	uint32_t check = val & m->mask;
	return check == m->value;
}

static struct {
	struct maskv32 ctmark;
} event_filter;

/*
 * can't use BSF during initial dump. Thus we filter manually
 * in dump_cb and then run nfct_filter_attach after the dump
 * has finished.
 */
static bool dumpcb_addr_is_lo(const struct nf_conntrack *ct)
{
	uint32_t addr;

	if (!cttop_options->ct_event_filter.loopback)
		return false;

	if (nfct_attr_is_set(ct, ATTR_IPV6_SRC)) {
		const uint32_t *res = nfct_get_attr(ct, ATTR_IPV6_SRC);
		int i;
		for (i = 0; i < 3; i++) {
			if (res[i])
				return false;
		}
		if (res[3] != htonl(1))
			return false;
		return true;
	}
	addr = nfct_get_attr_u32(ct, ATTR_IPV4_SRC);
	if ((addr & htonl(0x7f000000)) == htonl(0x7f000000))
		return true;
	return false;
}

static bool event_match_filter(const struct nf_conntrack *ct)
{
	uint32_t mark;

	mark = nfct_get_attr_u32(ct, ATTR_MARK);
	if (!maskv32_match(&event_filter.ctmark, mark))
		return false;

	/* XXX: match port, protocol, etc. */
	return true;
}

static void event_add_filter_ctmask(uint32_t mark, uint32_t mask)
{
	event_filter.ctmark.value = mark;
	event_filter.ctmark.mask = mask;
}

static void nfct2stats(struct nf_conntrack *ct, struct cttop_stats *stats)
{
	struct nfct_attr_grp_ctrs ctrs;
	int ret = nfct_get_attr_grp(ct, ATTR_GRP_ORIG_COUNTERS, &ctrs);
	if (ret == 0) {
		stats->out.bytes = ctrs.bytes;
		stats->out.packets = ctrs.packets;
	} else {
		memset(&stats->out, 0, sizeof(stats->out));
	}
	ret = nfct_get_attr_grp(ct, ATTR_GRP_REPL_COUNTERS, &ctrs);
	if (ret == 0) {
		stats->in.bytes = ctrs.bytes;
		stats->in.packets = ctrs.packets;
	} else {
		memset(&stats->in, 0, sizeof(stats->in));
	}
}

static int event_cb(enum nf_conntrack_msg_type type,
		    struct nf_conntrack *ct,
		    void *data)
{
	static int warn_once;
	struct cttop_stats stats;

	if (!event_match_filter(ct))
		return NFCT_CB_CONTINUE;

	switch (type) {
	case NFCT_T_NEW:
		ct_entry_new(ct);
		break;
	case NFCT_T_DESTROY:
		ct_entry_del(ct);
		break;
	case NFCT_T_UPDATE:
		nfct2stats(ct, &stats);
		ct_entry_update(ct, &stats);
	break;
	default:
		if (warn_once)
			break;
		warn_once = 1;
		fprintf(stderr, "%s: received unknown ct event type %d\n",
						__func__, (int) type);
	}
	(void) data;
	return NFCT_CB_CONTINUE;
}

static int dump_cb(enum nf_conntrack_msg_type type,
		    struct nf_conntrack *ct,
		    void *data)
{
	if (dumpcb_addr_is_lo(ct))
		return NFCT_CB_CONTINUE;

	return event_cb(type, ct, data);
}

static int query_cb(enum nf_conntrack_msg_type type,
		    struct nf_conntrack *ct,
		    void *data)
{
	static int warn_once;
	struct cttop_stats stats;

	switch (type) {
	case NFCT_T_UPDATE:
		nfct2stats(ct, &stats);
		ct_entry_update(ct, &stats);
	break;
	case NFCT_T_ERROR:
		fprintf(stderr, "%s: received NFCT_T_ERROR\n",
						__func__);
	break;
	default:
		if (warn_once)
			break;
		warn_once = 1;
		fprintf(stderr, "%s: received unknown ct event type %d\n",
						__func__, (int) type);
	}
	(void) data;
	return NFCT_CB_CONTINUE;
}


bool ctevent_query(struct nf_conntrack *ct)
{
	int ret;

	ret = nfct_query(nfct_query_h, NFCT_Q_GET, ct);
	if (ret == -1) {
		if (errno == EINTR || errno == EAGAIN || errno == ENOBUFS)
			return false;

		if (errno == ENOENT) {
			/*
			 * This happens e.g. if we missed a DELETE message,
			 * or if the delete message has been sent, but is still
			 * unprocessed.
			 */
			ct_entry_del(ct);
			return true;
		}
		perror("nfct_query");
		exit(1);
	}
	return true;
}

void ctevent_dispatch(void)
{
	int ret;

	ret = nfct_catch(nfct_handle);
	if (ret == -1) {
		if (errno == EINTR || errno == EAGAIN || errno == ENOBUFS)
			return;
		perror("nfct_catch");
		exit(1);
	}
	ct_entry_poll_counters_resume();
}

static void ctevent_exit(void)
{
	nfct_close(nfct_handle);
}

static struct nfct_handle *xnfct_open(uint8_t subsys, unsigned int subscribe)
{
	struct nfct_handle *h;

	h = nfct_open(subsys, subscribe);
	if (!h) {
		perror("nfct_open");
		exit(1);
	}
	return h;
}

static void fetch_existing_ctracks(void)
{
        u_int32_t family = AF_INET;
	int ret;

	/* need to use nfct_handle, so event_cb can filter marks */
        ret = nfct_query(nfct_handle, NFCT_Q_DUMP, &family);
	if (ret != 0) {
		perror("nfct_query");
		exit(1);
	}
	family = AF_INET6;
        ret = nfct_query(nfct_handle, NFCT_Q_DUMP, &family);
	if (ret != 0)
		perror("nfct_query");
}

static void setup_ct_lo_filter(int fd)
{
	struct nfct_filter *filter;
	static const struct nfct_filter_ipv4 filter_ipv4 = {
		.addr = 0x7f000001,
		.mask = 0xff000000,
	};
	static const struct nfct_filter_ipv6 filter_ipv6 = {
		.addr = { 0x0, 0x0, 0x0, 0x1 },
		.mask = { 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff },
	};

	filter = nfct_filter_create();
	if (!filter) {
		perror("nfct_create_filter");
		return;
	}

	nfct_filter_set_logic(filter, NFCT_FILTER_SRC_IPV4, NFCT_FILTER_LOGIC_NEGATIVE);
	nfct_filter_add_attr(filter, NFCT_FILTER_SRC_IPV4, &filter_ipv4);

	nfct_filter_set_logic(filter, NFCT_FILTER_SRC_IPV6, NFCT_FILTER_LOGIC_NEGATIVE);
	nfct_filter_add_attr(filter, NFCT_FILTER_SRC_IPV6, &filter_ipv6);

	if (nfct_filter_attach(fd, filter))
		perror("nfct_filter_attach");

	nfct_filter_destroy(filter);
}

int ctevent_init(void)
{
	event_add_filter_ctmask(cttop_options->ct_event_filter.ctmark,
					cttop_options->ct_event_filter.ctmask);

	nfct_handle = xnfct_open(CONNTRACK, NFCT_ALL_CT_GROUPS);
	nfct_callback_register(nfct_handle, NFCT_T_ALL, dump_cb, NULL);
	fetch_existing_ctracks();
	nfct_callback_unregister(nfct_handle);

	nfct_callback_register(nfct_handle, NFCT_T_ALL, event_cb, NULL);

	nfct_query_h = xnfct_open(CONNTRACK, 0);
	nfct_callback_register(nfct_query_h, NFCT_T_ALL, query_cb, NULL);

	if (cttop_options->ct_event_filter.loopback)
		setup_ct_lo_filter(nfct_fd(nfct_handle));

	nfnl_rcvbufsiz(nfct_nfnlh(nfct_handle), 2 * 1024  * 1024);

	atexit(ctevent_exit);
	return nfct_fd(nfct_handle);
}

