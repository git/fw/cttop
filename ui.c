/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#include <ctype.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <arpa/inet.h>

#include "ct_entry.h"
#include "ct_opts.h"
#include "ct_timer.h"
#include "ui_curses.h"
#include "statistics.h"

enum display_mode {
	DMODE_NORMAL,
	DMODE_HELP,
	DMODE_PROMPT,
	DMODE_FIELDS,
};

static enum display_mode display_mode;

static void ui_print_stats(void);
static void flush_stdout(void)
{
	fflush(stdout);
}

static const struct ui_ops ui_stdio_ops = {
	.ui_get_character = getchar,
	.ui_print_fmt = printf,
	.ui_print_change_fmt = printf,
	.ui_print_prompt_fmt = printf,
	.ui_refresh = flush_stdout,
};

static const struct ui_ops *ui_ops = &ui_stdio_ops;
static unsigned int linecount = 50;
static bool field_columns[__FIELD_MAX] = {
	[FIELD_L4PROTO] = true,
	[FIELD_SADDR] = true,
	[FIELD_DADDR] = true,
	[FIELD_INB] = true,
	[FIELD_OUTB] = true,
	[FIELD_INPKT] = false,
	[FIELD_OUTPKT] = false,
	[FIELD_AVG_INB] = true,
	[FIELD_AVG_OUTB] = true,
	[FIELD_SPORT] = true,
	[FIELD_DPORT] = true,
	[FIELD_CTMARK] = true,
	[FIELD_CTSECMARK] = false,
	[FIELD_FLOWS] = true,
	[FIELD_SRCCOUNT] = true,
};

#define FMT_ADDR "%39s"
#define FMT_PROTO "%7s"
#define FMT_PORT "%5"PRIu16
#define FMT_COUNTER "%8" PRIu64
#define FMT_PORT_NAME "%5s"
#define FMT_COUNTER_NAME "%8s"
#define FMT_MARK "%08"PRIx32
#define FMT_PROC_STAT_STR	"%-12s"
#define FMT_PROC_STAT_VALUE	"%8"PRIu32

#define	K_FACTOR	1024llu
#define	M_FACTOR	(K_FACTOR * K_FACTOR)
#define	G_FACTOR	(K_FACTOR * M_FACTOR)
#define T_FACTOR	(K_FACTOR * G_FACTOR)

#define	KI_FACTOR	1000llu
#define	MI_FACTOR	(KI_FACTOR * KI_FACTOR)
#define	GI_FACTOR	(KI_FACTOR * MI_FACTOR)
#define TI_FACTOR	(KI_FACTOR * GI_FACTOR)


#define SUFFIX_K(x)	((x) > M_FACTOR)
#define SUFFIX_M(x)	((x) > G_FACTOR)
#define SUFFIX_G(x)	((x) > T_FACTOR)
#define SUFFIX_T(x)	((x) > T_FACTOR * K_FACTOR)

#define SUFFIX_KI(x)	((x) > MI_FACTOR)
#define SUFFIX_MI(x)	((x) > GI_FACTOR)
#define SUFFIX_GI(x)	((x) > TI_FACTOR)
#define SUFFIX_TI(x)	((x) > TI_FACTOR * KI_FACTOR)

static void print_bcounter(uint64_t cnt)
{
	char suffix = ' ';

	if (cttop_options->batch_mode)
		goto print_raw;

	if (SUFFIX_T(cnt))
		suffix = 't';
	else if (SUFFIX_G(cnt))
		suffix = 'g';
	else if (SUFFIX_M(cnt))
		suffix = 'm';
	else if (SUFFIX_K(cnt))
		suffix = 'k';

	switch (suffix) {
	case 'k': cnt /= K_FACTOR; break;
	case 'm': cnt /= M_FACTOR; break;
	case 'g': cnt /= G_FACTOR; break;
	case 't': cnt /= T_FACTOR; break;
	}
 print_raw:
	ui_ops->ui_print_fmt(FMT_COUNTER "%c", cnt, suffix);
}

static void print_pcounter(uint64_t cnt)
{
	char suffix = ' ';

	if (cttop_options->batch_mode)
		goto print_raw;

	if (SUFFIX_TI(cnt))
		suffix = 'T';
	else if (SUFFIX_GI(cnt))
		suffix = 'G';
	else if (SUFFIX_MI(cnt))
		suffix = 'M';
	else if (SUFFIX_KI(cnt))
		suffix = 'K';

	switch (suffix) {
	case 'K': cnt /= KI_FACTOR; break;
	case 'M': cnt /= MI_FACTOR; break;
	case 'G': cnt /= GI_FACTOR; break;
	case 'T': cnt /= TI_FACTOR; break;
	}
 print_raw:
	ui_ops->ui_print_fmt(FMT_COUNTER "%c", cnt, suffix);
}

static const char *sort_order_to_str(enum sort_order s)
{
	const char *strtab[] = {
		[SORT_BYTES_TOTAL] = "bytes total",
		[SORT_BYTES_IN] = "bytes in",
		[SORT_BYTES_OUT] = "bytes out",

		[SORT_BYTES_AVG_TOTAL] = "bytes average",
		[SORT_BYTES_AVG_IN] = "bytes in average",
		[SORT_BYTES_AVG_OUT] = "bytes out average",
	};
	return strtab[s];
}

static const char *group_type_to_str(enum group_type g)
{
	const char *strtab[] = {
		[GROUP_NONE] = "all flows",
		[GROUP_SRC] =  "source address",
		[GROUP_DST] =  "destination address",
		[GROUP_DPORT] =  "source, destination, destination port",
		[GROUP_CTMARK] =  "conntrack mark",
	};
	return strtab[g];
}

static void ui_report_change_sort(enum sort_order s)
{
	const char *msg;

	msg = sort_order_to_str(s);
	ui_ops->ui_print_change_fmt("%s to: %s\n", "changed sort order", msg);
}

static void ui_report_change_grp(enum group_type g)
{
	const char *msg;

	msg = group_type_to_str(g);
	ui_ops->ui_print_change_fmt("%s to: %s\n", "changed group method", msg);
}

static void handle_sort_order(int what)
{
	enum sort_order new_order = __MAX_SORT_ORDERS;

	switch (what) {
	case 'a': new_order = SORT_BYTES_TOTAL; break;
	case 'b': new_order = SORT_BYTES_IN; break;
	case 'c': new_order = SORT_BYTES_OUT; break;
	case 'd': new_order = SORT_BYTES_AVG_IN; break;
	case 'e': new_order = SORT_BYTES_AVG_OUT; break;
	}

	ct_entry_set_sort_order(new_order);
}

static char *append_line(int ch)
{
	static char buf[64];
	static size_t i;

	if (!isprint(ch))
		goto done;

	buf[i] = ch;
	i++;
	if (i < sizeof(buf)-1)
		return NULL;
 done:
	buf[i] = 0;
	i = 0;
	return buf;
}

static void set_timer_delay(const char *str_const)
{
	char *end;
	struct timespec delay;
	uint64_t tmp;
	double timeval;
	static const double ns_per_s = NSEC_PER_SEC;

	errno = 0;
	timeval = strtod(str_const, &end);
	if (errno) {
		ui_ops->ui_print_change_fmt("-d: %s: %s", str_const, strerror(errno));
		return;
	}

	if (end == str_const || timeval == NAN || timeval < 0.01f) {
		ui_ops->ui_print_change_fmt("Not Valid");
		return;
	}

	delay.tv_sec = (uint64_t) timeval;
	if (delay.tv_sec >= 1.0)
		timeval -= (double) (delay.tv_sec);

	if (timeval < ns_per_s) {
		timeval *= ns_per_s;
		tmp = (uint64_t) timeval;
	} else {
		tmp = 0;
		delay.tv_sec++;
	}
	delay.tv_nsec = tmp;
	cttimer_set_interval(&delay);
}

static void show_help(void)
{
	if (!ui_ops->ui_help)
		return;

	display_mode = DMODE_HELP;
	ui_ops->ui_help();
}

static void show_fields(void)
{
	if (!ui_ops->ui_fields)
		return;
	display_mode = DMODE_FIELDS;
	ui_ops->ui_fields();
}

static bool handle_field_toggle(int ch)
{
	unsigned int i;
	ch = tolower(ch);

	for (i = 0; i < __FIELD_MAX; i++) {
		if (field_to_toggle_key(i) == ch) {
			field_toggle(i);
			return true;
		}
	}
	return false;
}

static void enter_normal_mode(void)
{
	display_mode = DMODE_NORMAL;
	ui_ops->ui_refresh();
	ui_redraw();
}

static void ui_do_redraw(void);
void ui_dispatch(void)
{
	int ch;
	unsigned int ret;
	char *line;

	ch = ui_ops->ui_get_character();
	if (ch == EOF)
		exit(0);

	switch (display_mode) {
	case DMODE_NORMAL: break;
	case DMODE_PROMPT:
		line = append_line(ch);
		if (!line)
			return;
		if (ui_ops->ui_prompt_finish)
			ui_ops->ui_prompt_finish();
		set_timer_delay(line);
	case DMODE_HELP: /* fallthrough */
		enter_normal_mode();
		return;
	case DMODE_FIELDS:
		if (!handle_field_toggle(ch)) {
			enter_normal_mode();
			return;
		}
		show_fields();
		return;
	}

	switch (ch) {
	case 'a':
	case 'b':
	case 'c':
		handle_sort_order(ch);
		break;
	case 'd':
		ui_ops->ui_print_prompt_fmt("Change delay to: ");
		display_mode = DMODE_PROMPT;
		return;
	case 'f':
		show_fields();
		return;
	case 'i': /* idle toggle */
		break;
	case 'g':
		ret = ct_entry_cycle_group();
		ui_report_change_grp(ret);
		break;
	case 'h':
		show_help();
		return;
	case 'H':
		ct_entry_toggle_group(GROUP_DPORT);
		ui_report_change_grp(GROUP_DPORT);
		break;
	case 'R':
		ct_entry_toggle_sort_flags(SORT_FLAG_REVERSE);
		ui_ops->ui_print_change_fmt("reverse sort\n");
		break;
	case 's':
		ret = ct_entry_cycle_sort();
		ui_report_change_sort(ret);
		break;
	case 'q':
		exit(0);
	default:
		ui_ops->ui_print_change_fmt("unknown command\n");
	}

	ui_do_redraw();
}

void ui_winch(void)
{
	if (ui_ops->ui_winch)
		ui_ops->ui_winch();
}

typedef void(*field_fmt_function)(struct ct_entry *e);
typedef void(*column_name_function)(void);


static const enum ctt_group_flowattr column_attr[] = {
	[FIELD_SADDR] = CTT_GRP_FLOWATTR_SRC,
	[FIELD_DADDR] = CTT_GRP_FLOWATTR_DST,
	[FIELD_DPORT] = CTT_GRP_FLOWATTR_DPORT,
	[FIELD_SPORT] = CTT_GRP_FLOWATTR_SPORT,
	[FIELD_L4PROTO] = CTT_GRP_FLOWATTR_PROTO,
	[FIELD_CTMARK] = CTT_GRP_FLOWATTR_CTMARK,
	[FIELD_CTSECMARK] = CTT_GRP_FLOWATTR_CTSECMARK,
	[FIELD_FLOWS] = CTT_GRP_FLOWATTR_FLOWS,
	[FIELD_SRCCOUNT] = CTT_GRP_FLOWATTR_SRCCOUNT,
};

static void field_fmt_proto(struct ct_entry *e)
{
	ui_ops->ui_print_fmt(FMT_PROTO, ct_entry_l4proto_str(e));
}

static void print_addr(unsigned int family, const void *addr)
{
	char str[INET6_ADDRSTRLEN];

	if (!inet_ntop(family, addr, str, sizeof(str))) {
		fprintf(stderr, "inet_ntop: %s (family %u)\n", strerror(errno), family);
		return;
	}
	ui_ops->ui_print_fmt(FMT_ADDR, str);
}

static void field_fmt_saddr(struct ct_entry *e)
{
	const struct nf_conntrack *ct = ct_entry_get_nfct(e);
	unsigned int family = nfct_get_attr_u8(ct, ATTR_L3PROTO);
	const void *addr;

	if (family == AF_INET)
		addr = nfct_get_attr(ct, ATTR_IPV4_SRC);
	else
		addr = nfct_get_attr(ct, ATTR_IPV6_SRC);
	print_addr(family, addr);
}

static void field_fmt_daddr(struct ct_entry *e)
{
	const struct nf_conntrack *ct = ct_entry_get_nfct(e);
	unsigned int family = nfct_get_attr_u8(ct, ATTR_L3PROTO);
	const void *addr;

	if (family == AF_INET)
		addr = nfct_get_attr(ct, ATTR_IPV4_DST);
	else
		addr = nfct_get_attr(ct, ATTR_IPV6_DST);
	print_addr(family, addr);
}

static void field_fmt_avginb(struct ct_entry *e)
{
	print_bcounter(ct_entry_avg_in(e));
}
static void field_fmt_avgoutb(struct ct_entry *e)
{
	print_bcounter(ct_entry_avg_out(e));
}
static void field_fmt_inb(struct ct_entry *e)
{
	const struct cttop_stats *stats;

	stats = ct_entry_stats(e);

	print_bcounter(stats->in.bytes);
}
static void field_fmt_outb(struct ct_entry *e)
{
	const struct cttop_stats *stats;

	stats = ct_entry_stats(e);

	print_bcounter(stats->out.bytes);
}
static void field_fmt_inpkt(struct ct_entry *e)
{
	const struct cttop_stats *stats;

	stats = ct_entry_stats(e);

	print_pcounter(stats->in.packets);
}
static void field_fmt_outpkt(struct ct_entry *e)
{
	const struct cttop_stats *stats;

	stats = ct_entry_stats(e);

	print_pcounter(stats->out.packets);
}

static void field_fmt_sport(struct ct_entry *e)
{
	const struct nf_conntrack *ct = ct_entry_get_nfct(e);
	uint16_t port = 0;

	if (nfct_attr_is_set(ct, ATTR_PORT_SRC) == 1) {
		port = nfct_get_attr_u16(ct, ATTR_PORT_SRC);
	} else if (nfct_attr_is_set(ct, ATTR_ICMP_TYPE) == 1) {
		port = nfct_get_attr_u8(ct, ATTR_ICMP_TYPE);
	}
	ui_ops->ui_print_fmt(FMT_PORT, ntohs(port));
}

static void field_fmt_dport(struct ct_entry *e)
{
	const struct nf_conntrack *ct = ct_entry_get_nfct(e);
	uint16_t port = 0;

	if (nfct_attr_is_set(ct, ATTR_PORT_DST) == 1) {
		port = nfct_get_attr_u16(ct, ATTR_PORT_DST);
	} else if (nfct_attr_is_set(ct, ATTR_ICMP_CODE) == 1) {
		port = nfct_get_attr_u8(ct, ATTR_ICMP_CODE);
	}
	ui_ops->ui_print_fmt(FMT_PORT, ntohs(port));
}

static void field_fmt_ctmark(struct ct_entry *e)
{
	const struct nf_conntrack *ct = ct_entry_get_nfct(e);
	uint32_t mark = nfct_get_attr_u32(ct, ATTR_MARK);
	ui_ops->ui_print_fmt(FMT_MARK, mark);
}
static void field_fmt_ctsecmark(struct ct_entry *e)
{
	const struct nf_conntrack *ct = ct_entry_get_nfct(e);
	uint32_t mark = nfct_get_attr_u32(ct, ATTR_SECMARK);
	ui_ops->ui_print_fmt(FMT_MARK, mark);
}
static void field_fmt_flows(struct ct_entry *e)
{
	ui_ops->ui_print_fmt(FMT_PORT, ct_entry_get_flows(e));
}
static void field_fmt_srccount(struct ct_entry *e)
{
	ui_ops->ui_print_fmt(FMT_PORT, ct_entry_get_srccount(e));
}

static void column_name_l4proto(void)
{
	ui_ops->ui_print_fmt(FMT_PROTO, "PROTO");
}
static void column_name_saddr(void)
{
	ui_ops->ui_print_fmt(FMT_ADDR, "SRC");
}
static void column_name_daddr(void)
{
	ui_ops->ui_print_fmt(FMT_ADDR, "DST");
}
static void column_name_inpkt(void)
{
	ui_ops->ui_print_fmt(" " FMT_COUNTER_NAME, "INPKT");
}
static void column_name_outpkt(void)
{
	ui_ops->ui_print_fmt(" " FMT_COUNTER_NAME, "OUTPKT");
}
static void column_name_inb(void)
{
	ui_ops->ui_print_fmt(" " FMT_COUNTER_NAME, "INB");
}
static void column_name_avginb(void)
{
	ui_ops->ui_print_fmt(" " FMT_COUNTER_NAME, "AVGINB");
}
static void column_name_outb(void)
{
	ui_ops->ui_print_fmt(" " FMT_COUNTER_NAME, "OUTB");
}
static void column_name_avgoutb(void)
{
	ui_ops->ui_print_fmt(" " FMT_COUNTER_NAME, "AVGOUTB");
}
static void column_name_sport(void)
{
	ui_ops->ui_print_fmt(FMT_PORT_NAME, "SPORT");
}
static void column_name_dport(void)
{
	ui_ops->ui_print_fmt(FMT_PORT_NAME, "DPORT");
}
static void column_name_ctmark(void)
{
	ui_ops->ui_print_fmt(FMT_COUNTER_NAME, "CTMARK");
}
static void column_name_ctsecmark(void)
{
	ui_ops->ui_print_fmt(FMT_COUNTER_NAME, "SECMARK");
}
static void column_name_flows(void)
{
	ui_ops->ui_print_fmt(FMT_PORT_NAME, "FLOWS");
}
static void column_name_srccount(void)
{
	ui_ops->ui_print_fmt(FMT_PORT_NAME, "SRCCOUNT");
}

static const struct {
	column_name_function name_func;
	field_fmt_function fmt_func;
	const char *shortname;
	const char *longname;
	char toggle;
} fields_data[] = {
	[FIELD_L4PROTO] = {
		.name_func = column_name_l4proto,
		.fmt_func = field_fmt_proto,
		.shortname= "PROTO",
		.longname = "Layer 4 Protocol",
		.toggle = 'l',
	},
	[FIELD_SADDR] = {
		.name_func = column_name_saddr,
		.fmt_func = field_fmt_saddr,
		.shortname= "SADDR",
		.longname = "Source IP Address",
		.toggle = 's',
	},
	[FIELD_DADDR] = {
		.name_func = column_name_daddr,
		.fmt_func = field_fmt_daddr,
		.shortname= "DADDR",
		.longname = "Destination IP Address",
		.toggle = 'd',
	},
	[FIELD_INB] = {
		.name_func = column_name_inb,
		.fmt_func = field_fmt_inb,
		.shortname= "INB",
		.longname = "Bytes Inbound",
		.toggle = 'e',
	},
	[FIELD_OUTB] = {
		.name_func = column_name_outb,
		.fmt_func = field_fmt_outb,
		.shortname= "OUTB",
		.longname = "Bytes Outbound",
		.toggle = 'b',
	},
	[FIELD_INPKT] = {
		.name_func = column_name_inpkt,
		.fmt_func = field_fmt_inpkt,
		.shortname= "INPKT",
		.longname = "Packets Inbound",
		.toggle = 'c',
	},
	[FIELD_OUTPKT] = {
		.name_func = column_name_outpkt,
		.fmt_func = field_fmt_outpkt,
		.shortname= "OUTPKT",
		.longname = "Packets Outbound",
		.toggle = 'g',
	},
	[FIELD_AVG_INB] = {
		.name_func = column_name_avginb,
		.fmt_func = field_fmt_avginb,
		.shortname= "AVGINB",
		.longname = "Average Bytes Inbound",
		.toggle = 'e',
	},
	[FIELD_AVG_OUTB] = {
		.name_func = column_name_avgoutb,
		.fmt_func = field_fmt_avgoutb,
		.shortname= "AVGOUTB",
		.longname = "Average Bytes Outbound",
		.toggle = 'f',
	},
	[FIELD_SPORT] = {
		.name_func = column_name_sport,
		.fmt_func = field_fmt_sport,
		.shortname= "SPORT",
		.longname = "Source Port Number",
		.toggle = 'o',
	},
	[FIELD_DPORT] = {
		.name_func = column_name_dport,
		.fmt_func = field_fmt_dport,
		.shortname= "DPORT",
		.longname = "Destination Port Number",
		.toggle = 'p',
	},
	[FIELD_CTMARK] = {
		.name_func = column_name_ctmark,
		.fmt_func = field_fmt_ctmark,
		.shortname= "CTMARK",
		.longname = "Conntrack Mark",
		.toggle = 'm',
	},
	[FIELD_CTSECMARK] = {
		.name_func = column_name_ctsecmark,
		.fmt_func = field_fmt_ctsecmark,
		.shortname= "CTSECMARK",
		.longname = "Conntrack Security Mark",
		.toggle = 'n',
	},
	[FIELD_FLOWS] = {
		.name_func = column_name_flows,
		.fmt_func = field_fmt_flows,
		.shortname= "FLOWS",
		.longname = "number of connections within group",
		.toggle = 'o',
	},
	[FIELD_SRCCOUNT] = {
		.name_func = column_name_srccount,
		.fmt_func = field_fmt_srccount,
		.shortname= "SRCCOUNT",
		.longname = "Number of unique source IP Addresses within group",
		.toggle = 'r',
	},
};

const char *field_to_shortname_str(enum field f)
{
	return fields_data[f].shortname;
}

const char *field_to_longname_str(enum field f)
{
	return fields_data[f].longname;
}

char field_to_toggle_key(enum field f)
{
	return fields_data[f].toggle;
}

void field_toggle(enum field f)
{
	field_columns[f] = !field_columns[f];
}

bool field_is_enabled(enum field f)
{
	return field_columns[f];
}

static void ui_print_line(struct ct_entry *e)
{
	int i, j;

	for (i = 0, j = 1; i < __FIELD_MAX ; i++, j++) {
		if (!field_columns[i] ||
		    !ct_entry_group_has_attr(column_attr[i]))
			continue;
		fields_data[i].fmt_func(e);
		if (j != __FIELD_MAX)
			ui_ops->ui_print_fmt(" ");
	}
	ui_ops->ui_print_fmt("\n");
}

static void ui_do_redraw(void)
{
	struct ct_entry *e;
	unsigned int i, j;

	for (i = 0, j = 1; i < __FIELD_MAX ; i++, j++) {
		if (!field_columns[i] ||
		    !ct_entry_group_has_attr(column_attr[i]))
			continue;
		fields_data[i].name_func();
		if (j != __FIELD_MAX)
			ui_ops->ui_print_fmt(" ");
	}
	ui_ops->ui_print_fmt("\n");

	for (i = 0; i < linecount; i++) {
		e = ct_entry_get_index(i);
		if (!e)
			break;
		ui_print_line(e);
	}
	ui_ops->ui_print_fmt("\n");
	ui_ops->ui_refresh();
}

static void ui_print_stats(void)
{
	static const enum proc_stat_nf_ct_fields stat_fmt[4][5] = {
{PROC_NFCT_ENTRIES,  PROC_NFCT_FOUND,      PROC_NFCT_SEARCHED, PROC_NFCT_SEARCHRESTART,PROC_NFCT_MAX},
{PROC_NFCT_NEW,      PROC_NFCT_INVALID,    PROC_NFCT_IGNORE,   PROC_NFCT_DELETE,       PROC_NFCT_DELETELIST},
{PROC_NFCT_INSERT,   PROC_NFCT_INSERTFAIL, PROC_NFCT_DROP,     PROC_NFCT_EARLYDROP,    PROC_NFCT_ICMPERR},
{PROC_NFCT_EXPECTNEW,PROC_NFCT_EXPECTCREAT,PROC_NFCT_EXPECTDEL,PROC_NFCT_MAX },
	};

	unsigned int i, j;
	if (cttop_options->batch_mode)
		return;

	for (i = 0; i < 4 ; i++) {
		for (j = 0; j < 5 ; j++) {
			if (stat_fmt[i][j] == PROC_NFCT_MAX)
				break;
			ui_ops->ui_print_fmt(FMT_PROC_STAT_STR" " FMT_PROC_STAT_VALUE " ",
				proc_nfct_get_attr_str(stat_fmt[i][j]),
				proc_nfct_get_attr_value(stat_fmt[i][j]));
			if (j < 5 && stat_fmt[i][j+1] != PROC_NFCT_MAX)
				ui_ops->ui_print_fmt(" ");
		}
		ui_ops->ui_print_fmt("\n");
	}
}

void ui_redraw(void)
{
	if (display_mode != DMODE_NORMAL)
		return;

	ui_print_stats();

	ui_ops->ui_print_change_fmt("\n");

	ui_do_redraw();
}

void ui_init(void)
{
	unsigned int extralines = 2;

	if (!cttop_options->batch_mode &&
	    isatty(0) && isatty(1)) {
		ui_ops = ui_curses_init();
		extralines += 4;
	} else {
		unsigned int i;
		for (i=0 ; i < __FIELD_MAX; i++)
			field_columns[i] = true;
		setvbuf(stdout, NULL, _IOFBF, 0);
	}

	if (ui_ops->ui_get_linecount) {
		linecount = ui_ops->ui_get_linecount();
		if (linecount > extralines)
			linecount-= extralines;
	}
	if (cttop_options->max_lines)
		linecount = cttop_options->max_lines;
}

void ui_exit(void)
{
}
