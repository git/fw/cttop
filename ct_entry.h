/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#ifndef CTTOP_CT_ENTRY_H_
#define CTTOP_CT_ENTRY_H_
#include <stdbool.h>
#include <inttypes.h>

#include <netinet/in.h>

#include <libnetfilter_conntrack/libnetfilter_conntrack.h>

enum sort_flags {
	SORT_FLAG_REVERSE = (1 << 0),
};

enum sort_order {
	SORT_BYTES_TOTAL,
	SORT_BYTES_IN,
	SORT_BYTES_OUT,

	SORT_BYTES_AVG_TOTAL,
	SORT_BYTES_AVG_IN,
	SORT_BYTES_AVG_OUT,

	__MAX_SORT_ORDERS
};

enum group_type {
	GROUP_NONE,	/* show single flows. default. */
	GROUP_SRC,	/* group src:any -> any:any */
	GROUP_DST,	/* group any:any -> dst:any */
	GROUP_DPORT,	/* group src:any -> dst:port */
	GROUP_CTMARK,	/* group by ctmark value */

	GROUP_MAX
};

enum ctt_group_flowattr {
	CTT_GRP_FLOWATTR_SRC = (1 << 0),
	CTT_GRP_FLOWATTR_DST = (1 << 1),
	CTT_GRP_FLOWATTR_SPORT = (1 << 2),
	CTT_GRP_FLOWATTR_DPORT = (1 << 3),
	CTT_GRP_FLOWATTR_PROTO = (1 << 4),
	CTT_GRP_FLOWATTR_CTMARK = (1 << 5),
	CTT_GRP_FLOWATTR_CTSECMARK = (1 << 6),
	CTT_GRP_FLOWATTR_FLOWS = (1 << 7),
	CTT_GRP_FLOWATTR_SRCCOUNT = (1 << 8),

	CTT_GRP_FLOWATTR_PORTS = CTT_GRP_FLOWATTR_SPORT |
				CTT_GRP_FLOWATTR_DPORT
};

struct cttop_counter {
	uint64_t packets;
	uint64_t bytes;
};

struct cttop_stats {
	struct cttop_counter in;
	struct cttop_counter out;
};


enum group_type ct_entry_cycle_group(void);
void ct_entry_toggle_group(enum group_type type);
bool ct_entry_group_has_attr(enum ctt_group_flowattr attr);

void ct_entry_toggle_sort_flags(unsigned int flags);
void ct_entry_set_sort_order(enum sort_order);
enum sort_order ct_entry_cycle_sort(void);

struct ct_entry;
const char *ct_entry_l4proto_str(const struct ct_entry *e);
unsigned int ct_entry_get_flows(const struct ct_entry *e);
unsigned int ct_entry_get_srccount(const struct ct_entry *e);
struct cttop_stats* ct_entry_stats(struct ct_entry *e);

uint64_t ct_entry_avg_in(const struct ct_entry *e);
uint64_t ct_entry_avg_out(const struct ct_entry *e);
struct ct_entry *ct_entry_get_index(unsigned int idx);

void ct_entry_new(struct nf_conntrack *ct);
void ct_entry_del(struct nf_conntrack *ct);
void ct_entry_update(struct nf_conntrack *ct, const struct cttop_stats *s);
const struct nf_conntrack *ct_entry_get_nfct(const struct ct_entry *e);

void ct_entry_poll_counters(void);
void ct_entry_poll_counters_resume(void);

void ct_entry_init(void);
#endif
