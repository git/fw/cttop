/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include <netdb.h>
#include <glib.h>

#include "ui.h"
#include "cttop.h"
#include "ct_acct.h"
#include "ct_entry.h"
#include "ct_opts.h"
#include "ct_events.h"
#include "ct_timer.h"
#include "jhash3.h"

struct list_head {
	GSList *list;
	GSList *tailp;
};

static struct {
	GSList *current;
	struct list_head completed;
	struct list_head new;
} counter_poll_stat;

#define MAX_ENTRIES	100
#define AVERAGE_SECONDS	3.0

struct ctt_group {
	unsigned int flow_attr;
	GPtrArray *parray;
	GHashTable *htable;
	gboolean (*equalfn)(gconstpointer, gconstpointer);
	guint (*hashfn)(gconstpointer);
	void (*valfreefn)(void *);
};

static uint32_t jhash_seed;

static enum sort_order sort_method = SORT_BYTES_TOTAL;
static enum sort_flags sort_flags;
static enum group_type group_active = GROUP_NONE;

enum status_bit {
	STATUS_QUERIED = 1,
	STATUS_IN_ARRAY = 2,
	STATUS_DESTROYED = 4,
	STATUS_MARK_INVALID = 8,
};

enum acct_dir {
	DIR_IN,
	DIR_OUT
};

struct byte_stats {
	uint64_t bytes[2];
};

struct ct_entry {
	struct cttop_stats stats;
	struct byte_stats avg;
	struct byte_stats avg_tmp;
	uint16_t tick_count;
	uint32_t flags;
	struct ct_acct acct;
	uint64_t modified_tick;
	char protoname[8];
	struct nf_conntrack *nfct;
};

static void list_append(struct list_head *h, GSList *append)
{
	if (h->tailp)
		h->tailp->next = append;
	else
		h->list = append;

	while (append->next)
		append = append->next;
	h->tailp = append;
}

static bool list_append_new(struct list_head *h, void *data)
{
	GSList *list = g_slist_alloc();
	if (!list) {
		perror("g_slist_append");
		return false;
	}
	list->data = data;
	list_append(h, list);
	return true;
}

static void list_null(struct list_head *h)
{
	h->list = NULL;
	h->tailp = NULL;
}

static bool ct_entry_has_flag(const struct ct_entry *e, enum status_bit bit)
{
	return (e->flags & bit) != 0;
}
static void ct_entry_set_flag(struct ct_entry *e, enum status_bit bit)
{
	e->flags |= bit;
}
static void ct_entry_unset_flag(struct ct_entry *e, enum status_bit bit)
{
	e->flags &= ~bit;
}

typedef gint(*sort_func)(const struct ct_entry *a, const struct ct_entry *b);

static gint ctentry_cmp_stats_u64(const uint64_t *a, const uint64_t *b)
{
	int ret;

	if (*a < *b)
		ret = 1;
	else if (*a > *b)
		ret = -1;
	else
		return 0;
	if (sort_flags == SORT_FLAG_REVERSE)
		return -ret;
	return ret;
}

static gint ctentry_cmp_stats_inoutb(const struct ct_entry *c1, const struct ct_entry *c2)
{
	uint64_t t1 = c1->stats.in.bytes + c1->stats.out.bytes;
	uint64_t t2 = c2->stats.in.bytes + c2->stats.out.bytes;
	return ctentry_cmp_stats_u64(&t1, &t2);
}

static gint ctentry_cmp_stats_inb(const struct ct_entry *c1, const struct ct_entry *c2)
{
	return ctentry_cmp_stats_u64(&c1->stats.in.bytes, &c2->stats.in.bytes);
}

static gint ctentry_cmp_stats_outb(const struct ct_entry *c1, const struct ct_entry *c2)
{
	return ctentry_cmp_stats_u64(&c1->stats.out.bytes, &c2->stats.out.bytes);
}

static gint ctentry_cmp_avg_inoutb(const struct ct_entry *c1, const struct ct_entry *c2)
{
	uint64_t t1 = c1->avg.bytes[DIR_IN] + c1->avg.bytes[DIR_OUT];
	uint64_t t2 = c2->avg.bytes[DIR_IN] + c2->avg.bytes[DIR_OUT];
	return ctentry_cmp_stats_u64(&t1, &t2);
}

static gint ctentry_cmp_avg_inb(const struct ct_entry *c1, const struct ct_entry *c2)
{
	return ctentry_cmp_stats_u64(&c1->avg.bytes[DIR_IN], &c2->avg.bytes[DIR_IN]);
}

static gint ctentry_cmp_avg_outb(const struct ct_entry *c1, const struct ct_entry *c2)
{
	return ctentry_cmp_stats_u64(&c1->avg.bytes[DIR_OUT], &c2->avg.bytes[DIR_OUT]);
}

static const sort_func sort_functions[] = {
	[SORT_BYTES_TOTAL] = ctentry_cmp_stats_inoutb,
	[SORT_BYTES_IN] = ctentry_cmp_stats_inb,
	[SORT_BYTES_OUT] = ctentry_cmp_stats_outb,

	[SORT_BYTES_AVG_TOTAL] = ctentry_cmp_avg_inoutb,
	[SORT_BYTES_AVG_IN] = ctentry_cmp_avg_inb,
	[SORT_BYTES_AVG_OUT] = ctentry_cmp_avg_outb,
};

static uint32_t do_uhash(const uint32_t *keys, size_t num_keys)
{
	return uhash_words(keys, num_keys, jhash_seed);
}

static uint32_t nfct_get_last_ipv6_word(const struct nf_conntrack *ct,
				const enum nf_conntrack_attr type)
{
	const uint32_t *tmp = nfct_get_attr(ct, type);
	if (!tmp) {
		perror("nfct_get_attr ATTR_IPV6");
		return 0;
	}
	return tmp[3];
}

static guint nfct_hash_dport(gconstpointer arg)
{
	const struct nf_conntrack *ct = arg;
	uint32_t scratch[3];
	unsigned int family;

	family = nfct_get_attr_u8(ct, ATTR_L3PROTO);
	scratch[0] = (family << 8) | nfct_get_attr_u8(ct, ATTR_L4PROTO);
	scratch[0] <<= 16;

	scratch[0] += nfct_get_attr_u16(ct, ATTR_PORT_DST);

	if (family == AF_INET) {
		scratch[1] = nfct_get_attr_u32(ct, ATTR_IPV4_SRC);
		scratch[2] = nfct_get_attr_u32(ct, ATTR_IPV4_DST);
	} else {
		scratch[1] = nfct_get_last_ipv6_word(ct, ATTR_IPV6_SRC);
		scratch[2] = nfct_get_last_ipv6_word(ct, ATTR_IPV6_DST);
	}

	return do_uhash(scratch, ARRAY_SIZE(scratch));
}

static guint nfct_hash_dst(gconstpointer arg)
{
	const struct nf_conntrack *ct = arg;
	unsigned int family = nfct_get_attr_u8(ct, ATTR_L3PROTO);
	const uint32_t *addr;

	if (family == AF_INET) {
		addr = nfct_get_attr(ct, ATTR_IPV4_DST);
		return do_uhash(addr, 1);
	}
	addr = nfct_get_attr(ct, ATTR_IPV6_DST);
	assert(addr);
	return do_uhash(addr, 4);
}
static guint nfct_hash_src(gconstpointer arg)
{
	const struct nf_conntrack *ct = arg;
	unsigned int family = nfct_get_attr_u8(ct, ATTR_L3PROTO);
	const uint32_t *addr;

	if (family == AF_INET) {
		addr = nfct_get_attr(ct, ATTR_IPV4_SRC);
		return do_uhash(addr, 1);
	}
	addr = nfct_get_attr(ct, ATTR_IPV6_SRC);
	assert(addr);
	return do_uhash(addr, 4);
}

static guint nfct_hash(gconstpointer arg)
{
	const struct nf_conntrack *ct = arg;
	uint32_t scratch[4];
	unsigned int family;

	family = nfct_get_attr_u8(ct, ATTR_L3PROTO);
	scratch[0] = nfct_get_attr_u16(ct, ATTR_PORT_DST) << 16 |
			nfct_get_attr_u16(ct, ATTR_PORT_SRC);
	scratch[1] = (family << 8) | nfct_get_attr_u8(ct, ATTR_L4PROTO);

	if (family == AF_INET) {
		scratch[2] = nfct_get_attr_u32(ct, ATTR_IPV4_SRC);
		scratch[3] = nfct_get_attr_u32(ct, ATTR_IPV4_DST);
	} else {
		scratch[2] = nfct_get_last_ipv6_word(ct, ATTR_IPV6_SRC);
		scratch[3] = nfct_get_last_ipv6_word(ct, ATTR_IPV6_DST);
	}

	return do_uhash(scratch, ARRAY_SIZE(scratch));
}

static guint nfct_hash_ctmark(gconstpointer arg)
{
	uint32_t mask = cttop_options->ctmark_group_mask;
	uint32_t val = nfct_get_attr_u32(arg, ATTR_MARK) & mask;
	return do_uhash(&val, 1);
}

static gint ctentry_nfct_cmp(gconstpointer a, gconstpointer b)
{
	const struct nf_conntrack *ct1 = a;
	const struct nf_conntrack *ct2 = b;

	return nfct_cmp(ct1, ct2, NFCT_CMP_ORIG);
}

static gboolean ctentry_dst_equal(gconstpointer a, gconstpointer b)
{
	const struct nf_conntrack *ct1 = a;
	const struct nf_conntrack *ct2 = b;
	unsigned int family;
	const uint32_t *a1, *a2;

	family = nfct_get_attr_u8(ct1, ATTR_L3PROTO);
	if (family != nfct_get_attr_u8(ct2, ATTR_L3PROTO))
		return false;

	if (family == AF_INET) {
		return nfct_get_attr_u32(ct1, ATTR_IPV4_DST) ==
			nfct_get_attr_u32(ct2, ATTR_IPV4_DST);
	}
	a1 = nfct_get_attr(ct1, ATTR_IPV6_DST);
	a2 = nfct_get_attr(ct2, ATTR_IPV6_DST);
	return (memcmp(a1, a2, sizeof(uint32_t) * 4)) == 0;
}

static gboolean ctentry_dport_equal(gconstpointer a, gconstpointer b)
{
	const struct nf_conntrack *ct1 = a;
	const struct nf_conntrack *ct2 = b;

	if (nfct_get_attr_u16(ct1, ATTR_PORT_DST) !=
	    nfct_get_attr_u16(ct2, ATTR_PORT_DST))
		return false;

	if (nfct_get_attr_u8(ct1, ATTR_L4PROTO) !=
	    nfct_get_attr_u8(ct2, ATTR_L4PROTO))
		return false;

	return ctentry_dst_equal(a, b);
}

static gboolean ctentry_src_equal(gconstpointer a, gconstpointer b)
{
	const struct nf_conntrack *ct1 = a;
	const struct nf_conntrack *ct2 = b;
	unsigned int family;
	const uint32_t *a1, *a2;

	family = nfct_get_attr_u8(ct1, ATTR_L3PROTO);
	if (family != nfct_get_attr_u8(ct2, ATTR_L3PROTO))
		return false;

	if (family == AF_INET) {
		return nfct_get_attr_u32(ct1, ATTR_IPV4_SRC) ==
			nfct_get_attr_u32(ct2, ATTR_IPV4_SRC);
	}
	a1 = nfct_get_attr(ct1, ATTR_IPV6_SRC);
	a2 = nfct_get_attr(ct2, ATTR_IPV6_SRC);
	return (memcmp(a1, a2, sizeof(uint32_t) * 4)) == 0;
}

static gboolean ctentry_ctmark_equal(gconstpointer a, gconstpointer b)
{
	const struct nf_conntrack *ct1 = a;
	const struct nf_conntrack *ct2 = b;
	uint32_t mask = cttop_options->ctmark_group_mask;

	return (nfct_get_attr_u32(ct1, ATTR_MARK) & mask) ==
			(nfct_get_attr_u32(ct2, ATTR_MARK) & mask);
}

static void ct_entry_free(void *p)
{
	struct ct_entry *e = p;
	nfct_destroy(e->nfct);
	free(e);
}

static struct ctt_group ctt_groups[] = {
	[GROUP_NONE] = {
		.flow_attr = CTT_GRP_FLOWATTR_SRC |
				CTT_GRP_FLOWATTR_DST |
				CTT_GRP_FLOWATTR_PROTO |
				CTT_GRP_FLOWATTR_PORTS |
				CTT_GRP_FLOWATTR_CTMARK |
				CTT_GRP_FLOWATTR_CTSECMARK,
		.equalfn = ctentry_nfct_cmp,
		.hashfn = nfct_hash,
	},
	[GROUP_SRC] = {
		.flow_attr = CTT_GRP_FLOWATTR_SRC|CTT_GRP_FLOWATTR_FLOWS,
		.equalfn = ctentry_src_equal,
		.hashfn = nfct_hash_src,
		.valfreefn = ct_entry_free,
	},
	[GROUP_DST] = {
		.flow_attr = CTT_GRP_FLOWATTR_DST|CTT_GRP_FLOWATTR_FLOWS,
		.equalfn = ctentry_dst_equal,
		.hashfn = nfct_hash_dst,
		.valfreefn = ct_entry_free,
	},
	[GROUP_DPORT] = {
		.flow_attr = CTT_GRP_FLOWATTR_SRC|CTT_GRP_FLOWATTR_DST|CTT_GRP_FLOWATTR_DPORT|CTT_GRP_FLOWATTR_PROTO|CTT_GRP_FLOWATTR_FLOWS,
		.equalfn = ctentry_dport_equal,
		.hashfn = nfct_hash_dport,
		.valfreefn = ct_entry_free,
	},
	[GROUP_CTMARK] = {
		.flow_attr = CTT_GRP_FLOWATTR_CTMARK | CTT_GRP_FLOWATTR_FLOWS | CTT_GRP_FLOWATTR_SRCCOUNT,
		.equalfn = ctentry_ctmark_equal,
		.hashfn = nfct_hash_ctmark,
		.valfreefn = ct_entry_free,
	},
};

static gint ctentry_sort_function(gconstpointer a, gconstpointer b)
{
	const struct ct_entry * const *p1 = a;
	const struct ct_entry * const *p2 = b;
	const struct ct_entry *c1 = *p1;
	const struct ct_entry *c2 = *p2;

	return sort_functions[sort_method](c1, c2);
}

static void ct_entry_l4proto_setstr(struct ct_entry *e, const struct nf_conntrack *ct)
{
	int count;
	unsigned int num;
	struct protoent *p;

	num = nfct_get_attr_u8(ct, ATTR_L4PROTO);
	p = getprotobynumber(num);
	if (!p)
		goto number;

	count = snprintf(e->protoname, sizeof(e->protoname), "%s", p->p_name);
	if (count < (int) sizeof(e->protoname))
		return;
 number:
	snprintf(e->protoname, sizeof(e->protoname), "pf-%d", num);
}

static struct ct_entry *ct_entry_last_in_array(struct ctt_group *grp)
{
	struct ct_entry *e = NULL;
	int lastidx = grp->parray->len - 1;

	if (lastidx >= 0)
		e = g_ptr_array_index(grp->parray, lastidx);

	return e;
}

static void add_to_array_cond(struct ctt_group *grp, struct ct_entry *e)
{
	struct ct_entry *last;
	GPtrArray *array = grp->parray;

	if (array->len > MAX_ENTRIES) {
		last = ct_entry_last_in_array(grp);

		if (ctentry_sort_function(&e, &last) < 0)
			return;
	}

	if (!ct_entry_has_flag(e, STATUS_IN_ARRAY)) {
		ct_entry_set_flag(e, STATUS_IN_ARRAY);
		g_ptr_array_add(array, e);
	}

	g_ptr_array_sort(array, ctentry_sort_function);

	if (array->len > MAX_ENTRIES) {
		last = ct_entry_last_in_array(grp);
		ct_entry_unset_flag(last, STATUS_IN_ARRAY);
		g_ptr_array_remove_index_fast(array, array->len - 1);
	}
}

/* initiate or resume counter polling cycle.
 */
void ct_entry_poll_counters(void)
{
	uint64_t ticks_now = cttimer_get_ticks();
	GSList *cur;
	unsigned int budget = 20;

	while ((cur = counter_poll_stat.current)) {
		struct ct_entry *entry = cur->data;

		if (ct_entry_has_flag(entry, STATUS_DESTROYED)) {
			counter_poll_stat.current = g_slist_next(counter_poll_stat.current);
			ct_entry_free(entry);
			cur->next = NULL;
			g_slist_free(cur);
			continue;
		}

		if (ct_entry_has_flag(entry, STATUS_QUERIED))
			break;

		if (entry->modified_tick == ticks_now)
			break;

		/* must set this before query is sent */
		ct_entry_set_flag(entry, STATUS_QUERIED);

		if (!ctevent_query(entry->nfct)) {
			ct_entry_unset_flag(entry, STATUS_QUERIED);
			break;
		}
		counter_poll_stat.current = g_slist_next(counter_poll_stat.current);
		cur->next = NULL;
		list_append(&counter_poll_stat.completed, cur);

		if (--budget == 0)
			break;
	}

	if (counter_poll_stat.current == NULL) {
		GSList *new = counter_poll_stat.new.list;
		if (new) {
			counter_poll_stat.new.tailp->next = counter_poll_stat.completed.list;
			list_null(&counter_poll_stat.new);
			counter_poll_stat.completed.list = new;
		} else {
			new = counter_poll_stat.completed.list;
		}

		counter_poll_stat.current = new;
		list_null(&counter_poll_stat.completed);
	}
}

void ct_entry_poll_counters_resume(void)
{
	if (counter_poll_stat.current && counter_poll_stat.completed.list)
		ct_entry_poll_counters();
}

static struct ct_entry *ct_entry_alloc(struct nf_conntrack *ct)
{
	struct ct_entry *e;
	e = malloc(sizeof(*e));
	if (!e) {
		perror("malloc");
		return NULL;
	}

	e->nfct = nfct_clone(ct);
	if (!e->nfct) {
		perror("nfct_clone");
		free(e);
		return NULL;
	}

	memset(&e->stats, 0, sizeof(e->stats));
	memset(&e->avg, 0, sizeof(e->avg));
	memset(&e->avg_tmp, 0, sizeof(e->avg_tmp));
	e->tick_count = 0;

	e->flags = 0;
	e->modified_tick = cttimer_get_ticks();

	ct_entry_l4proto_setstr(e, ct);
	return e;
}

static bool ct_entry_nfct_has_ports(const struct nf_conntrack *ct)
{
	return (nfct_attr_is_set(ct, ATTR_PORT_SRC) == 1 &&
		nfct_attr_is_set(ct, ATTR_PORT_DST) == 1);
}

static struct ct_entry *group_hash_lookup(struct ctt_group *grp, const struct nf_conntrack *ct)
{
	if (grp->flow_attr & CTT_GRP_FLOWATTR_PORTS &&
	    !ct_entry_nfct_has_ports(ct))
		return NULL;
	return g_hash_table_lookup(grp->htable, ct);
}

static void group_add(struct ctt_group *grp, struct nf_conntrack *ct)
{
	struct ct_entry *entry;

	entry = group_hash_lookup(grp, ct);
	if (entry) {
		ct_acct_add(ct, &entry->acct);
		return;
	}

	entry = ct_entry_alloc(ct);
	if (!entry)
		return;
	g_hash_table_insert(grp->htable, entry->nfct, entry);
	ct_acct_init(ct, &entry->acct, grp->flow_attr);
	add_to_array_cond(grp, entry);
}

static struct ct_entry *ct_entry_new2(struct nf_conntrack *ct)
{
	struct ct_entry *e;
	size_t i;

	e = ct_entry_alloc(ct);
	if (!e)
		return NULL;

	if (!list_append_new(&counter_poll_stat.new, e)) {
		free(e);
		return NULL;
	}

	ct_acct_init(ct, &e->acct, 0);

	g_hash_table_insert(ctt_groups[GROUP_NONE].htable, e->nfct, e);
	add_to_array_cond(&ctt_groups[GROUP_NONE], e);

	for (i=GROUP_NONE + 1; i < ARRAY_SIZE(ctt_groups); i++)
		group_add(&ctt_groups[i], e->nfct);

	return e;
}

void ct_entry_new(struct nf_conntrack *ct)
{
	ct_entry_new2(ct);
}

static struct cttop_stats ct_stats_delta(const struct cttop_stats *old, const struct cttop_stats *new)
{
	struct cttop_stats delta;
	static int warn_once;

	if (new->in.packets < old->in.packets ||
	    new->out.packets < old->out.packets) {
		if (!warn_once)
			fprintf(stderr, "packet counter shrunk (in %" PRIu64 " -> %" PRIu64 ","
							" out %" PRIu64 " -> %" PRIu64 ")\n",
				new->in.packets , old->in.packets, new->out.packets, old->out.packets);
		warn_once = 1;
		memset(&delta, 0, sizeof(delta));
		return delta;
	}

	delta.in.packets = new->in.packets - old->in.packets;
	delta.in.bytes = new->in.bytes - old->in.bytes;
	delta.out.packets = new->out.packets - old->out.packets;
	delta.out.bytes = new->out.bytes - old->out.bytes;

	return delta;
}

static void ct_counter_add(struct cttop_counter *counter, const struct cttop_counter *new)
{
	counter->packets += new->packets;
	counter->bytes += new->bytes;
}

static void ct_counter_sub(struct cttop_counter *counter, const struct cttop_counter *old)
{
	if (counter->packets > old->packets)
		counter->packets -= old->packets;
	if (counter->bytes > old->bytes)
		counter->bytes -= old->bytes;
}

static void ct_stats_add(struct cttop_stats *stats, const struct cttop_stats *new)
{
	ct_counter_add(&stats->in, &new->in);
	ct_counter_add(&stats->out, &new->out);
}

static void ct_stats_sub(struct cttop_stats *stats, const struct cttop_stats *new)
{
	ct_counter_sub(&stats->in, &new->in);
	ct_counter_sub(&stats->out, &new->out);
}

static uint64_t value_to_sec(uint64_t value, double ticks_per_sec)
{
	double bytes = value;
	bytes *= ticks_per_sec;
	return (uint64_t)  bytes;
}

static void update_average(struct ct_entry *e, const struct cttop_stats *delta)
{
	uint64_t now = cttimer_get_ticks();
	uint64_t tick_average;
	double ticks_per_sec = cttimer_get_ticks_per_sec();

	tick_average = AVERAGE_SECONDS * ticks_per_sec;
	if (tick_average == 0)
		tick_average = 1;

	e->tick_count += now - e->modified_tick;

	e->avg_tmp.bytes[DIR_IN]  += delta->in.bytes;
	e->avg_tmp.bytes[DIR_OUT] += delta->out.bytes;

	if (!e->tick_count)
		return;

	e->modified_tick = now;
	e->avg.bytes[DIR_IN] = value_to_sec(e->avg_tmp.bytes[DIR_IN], ticks_per_sec);
	e->avg.bytes[DIR_OUT] = value_to_sec(e->avg_tmp.bytes[DIR_OUT], ticks_per_sec);
	e->avg.bytes[DIR_IN] /= e->tick_count;
	e->avg.bytes[DIR_OUT] /= e->tick_count;

	if (e->tick_count >= tick_average) {
		memset(&e->avg_tmp, 0, sizeof(e->avg_tmp));
		e->tick_count = 0;
	}
}

static void group_update(struct ctt_group *grp, const struct nf_conntrack *ct,
			const struct cttop_stats *delta)
{
	struct ct_entry *entry = group_hash_lookup(grp, ct);
	if (entry) {
		ct_stats_add(&entry->stats, delta);
		update_average(entry, delta);
		add_to_array_cond(grp, entry);
	}
}

static void ct_entry_update_stats(struct ct_entry *e, const struct cttop_stats *s)
{
	struct cttop_stats delta = ct_stats_delta(&e->stats, s);
	size_t i;

	for (i=GROUP_NONE;i < ARRAY_SIZE(ctt_groups); i++)
		group_update(&ctt_groups[i], e->nfct, &delta);
	e->stats = *s;
}

static void group_del(struct ctt_group *grp, const struct nf_conntrack *ct,
		struct cttop_stats *old_stats);
static void move_mark_group(struct nf_conntrack *ct, struct ct_entry *e)
{
	uint32_t new, old, mask;
	unsigned int i;

	mask = cttop_options->ctmark_group_mask;

	old = nfct_get_attr_u32(e->nfct, ATTR_MARK) & mask;
	new = nfct_get_attr_u32(ct, ATTR_MARK) & mask;

	if (old == new)
		return;

	group_del(&ctt_groups[GROUP_CTMARK], e->nfct, &e->stats);
	group_add(&ctt_groups[GROUP_CTMARK], ct);
	group_update(&ctt_groups[GROUP_CTMARK], ct, &e->stats);

	if (!cttop_options->ctmark_force || new == 0)
		return;

	for (i=GROUP_NONE + 1; i < GROUP_MAX; i++) {
		uint32_t mark;

		if (i == GROUP_CTMARK)
			continue;

		e = group_hash_lookup(&ctt_groups[i], ct);
		assert(e);
		if (!e || ct_entry_has_flag(e, STATUS_MARK_INVALID))
			continue;

		old = nfct_get_attr_u32(e->nfct, ATTR_MARK) & mask;
		if (old == new)
			continue;
		mark = nfct_get_attr_u32(e->nfct, ATTR_MARK) & ~mask;
		if (old == 0)
			mark |= new;
		else
			ct_entry_set_flag(e, STATUS_MARK_INVALID);
		nfct_set_attr_u32(e->nfct, ATTR_MARK, mark);
	}
}

void ct_entry_update(struct nf_conntrack *ct, const struct cttop_stats *s)
{
	struct ct_entry *e = g_hash_table_lookup(
			ctt_groups[GROUP_NONE].htable, ct);
	if (!e) {
		e = ct_entry_new2(ct);
		if (!e)
			return;
	} else {
		move_mark_group(ct, e);
		nfct_copy(e->nfct, ct, NFCT_CP_META);
	}

	if (s->in.packets || s->out.packets)
		ct_entry_update_stats(e, s);

	ct_entry_unset_flag(e, STATUS_QUERIED);
}

static void group_array_del(struct ctt_group *grp, const struct ct_entry *e)
{
	guint i, len = grp->parray->len;
	struct ct_entry *search;

	for (i=0; i < len; i++) {
		search = g_ptr_array_index(grp->parray, i);

		if (search == e) {
			g_ptr_array_remove_index_fast(grp->parray, i);
			break;
		}
	}
}

static void group_del(struct ctt_group *grp, const struct nf_conntrack *ct,
		struct cttop_stats *old_stats)
{
	struct ct_entry *e;

	e = g_hash_table_lookup(grp->htable, ct);
	if (!e)
		return;

	if (ct_acct_del(ct, &e->acct)) {
		group_array_del(grp, e);
		g_hash_table_remove(grp->htable, ct);
		return;
	}

	ct_stats_sub(&e->stats, old_stats);
}

void ct_entry_del(struct nf_conntrack *ct)
{
	size_t i;
	struct ct_entry *e;

	e = g_hash_table_lookup(ctt_groups[GROUP_NONE].htable, ct);
	if (!e)
		return;

	assert(!ct_entry_has_flag(e, STATUS_DESTROYED));

	ct_entry_set_flag(e, STATUS_DESTROYED);

	for (i=GROUP_NONE + 1; i < ARRAY_SIZE(ctt_groups); i++) {
		if (ctt_groups[i].flow_attr & CTT_GRP_FLOWATTR_PORTS) {
			if (ct_entry_nfct_has_ports(ct))
				group_del(&ctt_groups[i], ct, &e->stats);
			continue;
		}
		group_del(&ctt_groups[i], ct, &e->stats);
	}
	group_del(&ctt_groups[GROUP_NONE], ct, &e->stats);
}

static void rebuild_array_cb(gpointer key, gpointer value, gpointer user_data)
{
	struct ct_entry *e = value;
	struct ctt_group *grp = user_data;

	ct_entry_unset_flag(e, STATUS_IN_ARRAY);
	add_to_array_cond(grp, e);
	(void) key;
}

static void group_rebuild_array(struct ctt_group *grp)
{
	g_ptr_array_set_size(grp->parray, 0);

	g_hash_table_foreach(grp->htable,
				rebuild_array_cb, grp);
}

static void group_rebuild_array_cond(struct ctt_group *grp)
{
	g_ptr_array_sort(grp->parray, ctentry_sort_function);
}

const struct nf_conntrack *ct_entry_get_nfct(const struct ct_entry *e)
{
	return e->nfct;
}

struct ct_entry *ct_entry_get_index(unsigned int idx)
{
	GPtrArray *array;
	if (idx == 0)
		group_rebuild_array_cond(&ctt_groups[group_active]);

	array = ctt_groups[group_active].parray;

	if (idx >= array->len)
		return NULL;
	return g_ptr_array_index(array, idx);
}

const char *ct_entry_l4proto_str(const struct ct_entry *e)
{
	return e->protoname;
}

struct cttop_stats* ct_entry_stats(struct ct_entry *e)
{
	return &e->stats;
}

uint64_t ct_entry_avg_in(const struct ct_entry *e)
{
	return e->avg.bytes[DIR_IN];
}

uint64_t ct_entry_avg_out(const struct ct_entry *e)
{
	return e->avg.bytes[DIR_OUT];
}

void ct_entry_toggle_sort_flags(unsigned int flags)
{
	bool changed = false;

	if (flags & SORT_FLAG_REVERSE && !(sort_flags & SORT_FLAG_REVERSE)) {
		changed = true;
		sort_flags |= SORT_FLAG_REVERSE;
	} else if (sort_flags & SORT_FLAG_REVERSE) {
		changed = true;
		sort_flags &= ~SORT_FLAG_REVERSE;
	}
	if (changed)
		group_rebuild_array(&ctt_groups[group_active]);
}

void ct_entry_toggle_group(enum group_type type)
{
	if (type >= GROUP_MAX)
		return;

	if (type != group_active) {
		group_active = type;
	} else {
		group_active = GROUP_NONE;
	}

	if (ctt_groups[group_active].parray == NULL)
	/* can be called via option parsing before any entries exist */
		return;
	group_rebuild_array(&ctt_groups[group_active]);
}

enum group_type ct_entry_cycle_group(void)
{
	enum group_type next = group_active;

	next++;
	if (next == GROUP_MAX)
		next = GROUP_NONE;
	group_active = next;
	return next;
}

void ct_entry_set_sort_order(enum sort_order order)
{
	if (order < __MAX_SORT_ORDERS && order != sort_method) {
		sort_method = order;
		group_rebuild_array(&ctt_groups[group_active]);
	}
}

enum sort_order ct_entry_cycle_sort(void)
{
	enum sort_order next = sort_method;

	next++;
	if (next == __MAX_SORT_ORDERS)
		next = 0;
	ct_entry_set_sort_order(next);
	return next;
}

static void list_free(GSList *list)
{
	GSList *tmp = list;

	for (; list ; list = g_slist_next(list))
		ct_entry_free(list->data);
	g_slist_free(tmp);
}

static void ct_entry_exit(void)
{
	size_t i;

	endprotoent();

	for (i=0; i < ARRAY_SIZE(ctt_groups); i++) {
		g_ptr_array_free(ctt_groups[i].parray, TRUE);
		g_hash_table_destroy(ctt_groups[i].htable);
	}

	list_free(counter_poll_stat.current);
	list_free(counter_poll_stat.new.list);
	list_free(counter_poll_stat.completed.list);
}

bool ct_entry_group_has_attr(enum ctt_group_flowattr attr)
{
	return (ctt_groups[group_active].flow_attr & attr) == attr;
}

unsigned int ct_entry_get_flows(const struct ct_entry *e)
{
	return ct_acct_get_flow_count(&e->acct);
}

unsigned int ct_entry_get_srccount(const struct ct_entry *e)
{
	return ct_acct_get_saddr_count(&e->acct);
}


void ct_entry_init(void)
{
	size_t i;

	jhash_seed = random();

	setprotoent(1);

	for (i=0; i < ARRAY_SIZE(ctt_groups); i++) {
		ctt_groups[i].htable = g_hash_table_new_full(
			ctt_groups[i].hashfn, ctt_groups[i].equalfn,
			NULL, ctt_groups[i].valfreefn);
		if (!ctt_groups[i].htable) {
			perror("hash table initialization");
			exit(1);
		}
		ctt_groups[i].parray = g_ptr_array_new();
		if (!ctt_groups[i].parray) {
			perror("array initialization");
			exit(1);
		}
		if (cttop_options->ctmark_force)
			ctt_groups[i].flow_attr |= CTT_GRP_FLOWATTR_CTMARK;
	}

	atexit(ct_entry_exit);
}

