/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#include <ctype.h>
#include <curses.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "cttop.h"
#include "ct_opts.h"
#include "ct_timer.h"
#include "ui_curses.h"

static WINDOW* window;
static int row, col;

#define STATUS_MSG_ROW		4
#define MSG_VIS_SECONDS	2.0		/* number of seconds a status message is visible */

static void ui_curses_winch(void)
{
	endwin();
	refresh();
	getmaxyx(stdscr, row, col);
}

static int report_change(const char *fmt, ... );

static void ui_curses_help(void)
{
	static const char helpmsg[] = "Help for Interactive Commands - cttop version " PACKAGE_VERSION "\n"
				      "System: Delay %u.%u secs.\n\n"
				      "f:\t. Fields/Columns: 'f' add or remove\n"
				      "R,H:\t. Toggle: normal/reverse sort; 'H' hide source port\n"
				      "g:\t. Toggle group views\n"
				      "s:\t. Toggle sort modes\n"
				      "\n"
				      "d:\tSet Update Interval\n"
				      "q:\tQuit.\n\n"
				      "Press any other key to continue\n"
	;
	struct timespec ts = cttimer_get_interval();

	mvprintw(0, 0, helpmsg, ts.tv_sec, ts.tv_nsec / (NSEC_PER_SEC / 100));
	clrtobot();
	refresh();
}

static void ui_curses_fields(void)
{
	char curfields[__FIELD_MAX+1];
	unsigned int i;

	curfields[__FIELD_MAX] = 0;
	for (i = 0; i < __FIELD_MAX; i++) {
		curfields[i] = field_to_toggle_key(i);
		if (field_is_enabled(i))
			curfields[i] = toupper(curfields[i]);
	}
	move(0, 0);
	printw("Current Fields: ");
	attron(A_REVERSE);
	printw("%s", curfields);
	attroff(A_REVERSE);
	printw("\nToggle fields via field letter, type any other key to return\n\n");

	for (i = 0; i < __FIELD_MAX; i++) {
		char key = field_to_toggle_key(i);
		char enabled;

		if (field_is_enabled(i)) {
			enabled = '*';
			key = toupper(key);
		} else {
			key = tolower(key);
			enabled = ' ';
		}
		printw("%c %c: %s\t= %s\n", enabled, key, field_to_shortname_str(i), field_to_longname_str(i));
	}

	clrtobot();
	refresh();
}

static void prompt_finish(void)
{
	noecho();
}
static int print_prompt(const char *fmt, ... )
{
	char buf[256];
	va_list ap;
	int ret = 0;

	va_start(ap, fmt);
	ret = vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);
	mvprintw(STATUS_MSG_ROW, 0," %s", buf);
	clrtoeol();
	echo();
	refresh();

	return ret;
}

static int int_curses_getchar(void)
{
	return getch();
}

static void ui_curses_exit(void)
{
	nocbreak();
	endwin();
}

static void ui_curses_refresh(void)
{
	clrtobot();
	refresh();
	move(0, 0);
}

static unsigned int get_rowcount(void)
{
	int rows, cols;
	getmaxyx(stdscr, rows, cols);
	return cols;
}

static unsigned int get_linecount(void)
{
	int rows, cols;
	getmaxyx(stdscr, rows, cols);
	return rows;
}

static int report_change(const char *fmt, ... )
{
	char buf[256];
	va_list ap;
	static uint64_t last_tick;
	int ret = 0;
	uint32_t min_msg_ticks;

	min_msg_ticks = cttimer_get_ticks_per_sec() * MSG_VIS_SECONDS;

	if (fmt[0] == '\n') {
		if (last_tick + min_msg_ticks >= cttimer_get_ticks())
			move(STATUS_MSG_ROW+1, 0);
		else
			mvprintw(STATUS_MSG_ROW, 0,"\n");
		return ret;
	}

	last_tick = cttimer_get_ticks();
	va_start(ap, fmt);
	ret = vsnprintf(buf, sizeof(buf), fmt, ap);
	va_end(ap);
	attron(A_REVERSE);
	mvprintw(STATUS_MSG_ROW, 1," %s", buf);
	clrtoeol();
	attroff(A_REVERSE);
	refresh();
	return ret;
}

static const struct ui_ops ui_curses_ops = {
	.ui_help = ui_curses_help,
	.ui_fields = ui_curses_fields,
	.ui_get_character = int_curses_getchar,
	.ui_print_fmt = printw,
	.ui_print_change_fmt = report_change,
	.ui_print_prompt_fmt = print_prompt,
	.ui_prompt_finish = prompt_finish,
	.ui_winch = ui_curses_winch,
	.ui_refresh = ui_curses_refresh,
	.ui_get_rowcount = get_rowcount,
	.ui_get_linecount = get_linecount,
};

const struct ui_ops *ui_curses_init(void)
{
	use_env(true);
	window = initscr();
	cbreak();
	noecho();
	atexit(ui_curses_exit);
	ui_curses_winch();
	return &ui_curses_ops;
}

