#include <stdint.h>
#include <sys/time.h>
#include <sys/timerfd.h>

uint64_t cttimer_get_ticks(void);

/* ticks per second. default timer ticks every 2 seconds (0.5 ticks/s) */
double cttimer_get_ticks_per_sec(void);

void cttimer_set_interval(const struct timespec *it_interval);
struct timespec cttimer_get_interval(void);
int cttimer_init(void);
void cttimer_dispatch(void);

#define NSEC_PER_SEC       (1000ULL * 1000ULL * 1000ULL)
