/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#define _GNU_SOURCE
#include <unistd.h>

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <errno.h>


#include "cttop.h"
#include "ui.h"

static int signalpipe[2];

static const int signals_catch[] = {
	SIGINT, SIGQUIT, SIGTERM, SIGHUP, SIGWINCH,
};

static void sig_block(int sig)
{
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, sig);
	sigprocmask(SIG_BLOCK, &set, NULL);
}

static void sig_unblock(int sig)
{
	sigset_t set;

	sigemptyset(&set);
	sigaddset(&set, sig);
	sigprocmask(SIG_UNBLOCK, &set, NULL);
}

static void handler(int sig)
{
	if (write(signalpipe[1], &sig, sizeof(sig)) > 0)
		sig_block(sig);
}

static void sig_handle(int sigv)
{
	switch (sigv) {
	case SIGQUIT:
		fputs("got SIGQUIT, exiting\n", stderr);
		exit(0);
	case SIGINT:
		fputs("got SIGINT, exiting\n", stderr);
		exit(0);
	case SIGHUP:
	case SIGTERM:
		exit(0);
	case SIGWINCH:
		ui_winch();
		break;
	default:
		fprintf(stderr, "BUG: unknown signal %d\n", sigv);
		exit(1);
	}
	sig_unblock(sigv);
}

static void sig_handler_bh(int fd)
{
	ssize_t ret;
	int sig;

	ret = read(fd, &sig, sizeof(sig));
	if (ret > 0) {
		sig_handle(sig);
		return;
	}
	if (errno == EAGAIN || errno == EINTR)
		return;
	perror("reading from signal pipe");
	exit(1);
}

void ctsignal_dispatch(void)
{
	sig_handler_bh(signalpipe[0]);
}

int ctsignal_init(void)
{
	struct sigaction saction;
	size_t i;

	if (pipe2(signalpipe, O_NONBLOCK|O_CLOEXEC)) {
		perror("ctsignal_init: pipe2");
		exit(1);
	}

	memset(&saction, 0, sizeof(saction));
	saction.sa_handler = handler;
	saction.sa_flags |= SA_RESTART;

	for (i=0; i < ARRAY_SIZE(signals_catch) ; i++)
		sigaction(signals_catch[i], &saction, NULL);

	return signalpipe[0];
}

void ctsignal_exit(void)
{
	struct sigaction saction;
	size_t i;

	memset(&saction, 0, sizeof(saction));
	saction.sa_handler = SIG_DFL;

	for (i=0; i < ARRAY_SIZE(signals_catch) ; i++)
		sigaction(signals_catch[i], &saction, NULL);

	close(signalpipe[1]);
	ctsignal_dispatch();
	close(signalpipe[0]);
}

