#include <stdint.h>

enum proc_stat_nf_ct_fields {
	PROC_NFCT_ENTRIES,
	PROC_NFCT_SEARCHED,
	PROC_NFCT_FOUND,
	PROC_NFCT_NEW,
	PROC_NFCT_INVALID,
	PROC_NFCT_IGNORE,
	PROC_NFCT_DELETE,
	PROC_NFCT_DELETELIST,
	PROC_NFCT_INSERT,
	PROC_NFCT_INSERTFAIL,
	PROC_NFCT_DROP,
	PROC_NFCT_EARLYDROP,
	PROC_NFCT_ICMPERR,
	PROC_NFCT_EXPECTNEW,
	PROC_NFCT_EXPECTCREAT,
	PROC_NFCT_EXPECTDEL,
	PROC_NFCT_SEARCHRESTART,

	PROC_NFCT_MAX
};

uint32_t proc_nfct_get_attr_value(enum proc_stat_nf_ct_fields field);
const char *proc_nfct_get_attr_str(enum proc_stat_nf_ct_fields field);

