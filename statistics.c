/*
 * (C) 2011 Florian Westphal <fw@strlen.de>
 *
 * /proc parsing code taken from conntrack-tools, conntrack.c:
 *
 * (C) 2005-2008 by Pablo Neira Ayuso <pablo@netfilter.org>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "statistics.h"

#ifndef CT_STATS_PROC
#define CT_STATS_PROC "/proc/net/stat/nf_conntrack"
#endif

/* As of 2.6.29, we have 16 entries, this is enough */
#ifndef CT_STATS_ENTRIES_MAX
#define CT_STATS_ENTRIES_MAX 64
#endif

/* maximum string length currently is 13 characters */

static uint32_t proc_fields[PROC_NFCT_MAX];

static void update_proc_conntrack_stats(void)
{
	int ret = 0;
	FILE *fd;
	char buf[4096], *token, *nl;
	unsigned int i, max;

	fd = fopen(CT_STATS_PROC, "r");
	if (fd == NULL)
		return;

	if (fgets(buf, sizeof(buf), fd) == NULL) {
		ret = -1;
		goto out_err;
	}

	/* trim off trailing \n */
	nl = strchr(buf, '\n');
	while (nl != NULL) {
		*nl = '\0';
		nl = strchr(buf, '\n');
	}
	token = strtok(buf, " ");
	for (i=0; token != NULL && i<CT_STATS_ENTRIES_MAX; i++)
		token = strtok(NULL, " ");
	max = i;

	if (fgets(buf, sizeof(buf), fd) == NULL) {
		ret = -1;
		goto out_err;
	}

	nl = strchr(buf, '\n');
	while (nl != NULL) {
		*nl = '\0';
		nl = strchr(buf, '\n');
	}
	token = strtok(buf, " ");
	for (i=0; token != NULL && i < PROC_NFCT_MAX ; i++) {
		proc_fields[i] = (uint32_t) strtol(token, (char**) NULL, 16);
		token = strtok(NULL, " ");
	}

out_err:
	fclose(fd);
}

uint32_t proc_nfct_get_attr_value(enum proc_stat_nf_ct_fields field)
{
	if (field == 0)
		update_proc_conntrack_stats();

	if (field < PROC_NFCT_MAX)
		return proc_fields[field];
	return 0;
}

const char*proc_nfct_get_attr_str(enum proc_stat_nf_ct_fields field)
{
	switch (field) {
	case PROC_NFCT_ENTRIES: return "Entries:";
	case PROC_NFCT_SEARCHED: return "Searched:";
	case PROC_NFCT_FOUND: return "Found:";
	case PROC_NFCT_NEW: return "New:";
	case PROC_NFCT_INVALID: return "Invalid:";
	case PROC_NFCT_IGNORE: return "Ignore:";
	case PROC_NFCT_DELETE: return "Delete:";
	case PROC_NFCT_DELETELIST: return "Delete list:";
	case PROC_NFCT_INSERT: return "Insert:";
	case PROC_NFCT_INSERTFAIL: return "Failed:";
	case PROC_NFCT_DROP: return "Drop:";
	case PROC_NFCT_EARLYDROP: return "Early drop:";
	case PROC_NFCT_ICMPERR: return "ICMP error:";
	case PROC_NFCT_EXPECTNEW: return "Expect New:";
	case PROC_NFCT_EXPECTCREAT: return "Create:";
	case PROC_NFCT_EXPECTDEL: return "Delete:";
	case PROC_NFCT_SEARCHRESTART: return "Restarts:";
	case PROC_NFCT_MAX: break;
	}
	return "Internal Error";
}



