/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cttop.h"
#include "ct_opts.h"
#include "ct_entry.h"

/* access to struct ctt_opts */
const struct ctt_opts *cttop_options;

static struct ctt_opts ctt_opts;

static const char optstring[]="bd:g:hn:m:";
static const struct option ctt_longopts[] = {
	{
		.name = "batch",
		.has_arg = 0,
		.val = 'b',
	},{
		.name = "ctmark",
		.has_arg = 1,
		.val = 'm',
	},{
		.name = "ctmark-groupmask",
		.has_arg = 1,
		.val = 'M',
	},{
		.name = "ctmark-force",
		.has_arg = 0,
		.val = 'F',
	},{
		.name = "delay",
		.has_arg = 1,
		.val = 'd',
	},{
		.name = "filter-loopback",
		.has_arg = 0,
		.val = 'L',
	},{
		.name = "group",
		.has_arg = 1,
		.val = 'g',
	},{
		.name = "help",
		.has_arg = 0,
		.val = 'h',
	},{
		.name = "number",
		.has_arg = 1,
		.val = 'n',
	},{
		.name = "max-lines",
		.has_arg = 1,
		.val = 'E',
	},{
		.name = 0, /* end */
	}
};

static const struct { enum group_type type; const char *name; } group_symtab[] = {
	{ GROUP_NONE, "NONE" },
	{ GROUP_SRC, "SRC" },
	{ GROUP_DST, "DST" },
	{ GROUP_DPORT, "DPORT" },
	{ GROUP_CTMARK, "CTMARK" },
};

static void fputs_group_names(FILE *f)
{
	size_t i;
	for (i = 0; i < ARRAY_SIZE(group_symtab); i++)
		fprintf(f, "%s\n", group_symtab[i].name);
}

static void usage(FILE *fp)
{
	fputs("usage: ", fp);
	fputs("cttop -bh -d delay -n iterations -g GROUPNAME\n", fp);
	fputs("\t--ctmark-groupmask <mask> (-g CTMARK only): aggregate by given mask\n", fp);
	fputs("\t--ctmark <mark>/<mask>: only display connections matching <mark>\n", fp);
	fputs("\t--filter-loopback: exclude connections from/to 127.0.0.1/::\n", fp);
	fputs("\t--ctmark-force: display ctmark in all groups, if equal\n", fp);
	fputs("\t--max-lines <lines>: do not display more than <lines> of output\n", fp);
}

static enum group_type groupname_to_enum(const char *name)
{
	size_t i;

	for (i = 0; i < ARRAY_SIZE(group_symtab); i++) {
		if (strcasecmp(group_symtab[i].name, name) == 0)
			return group_symtab[i].type;
	}

	fprintf(stderr, "no such grouping method \"%s\", known groupnames are:\n", name);
	fputs_group_names(stderr);
	exit(1);
}

static unsigned long str_to_ulong(const char *nptr, char **end, int base)
{
	unsigned long res;

	errno = 0;
	res = strtoul(nptr, end, base);

	if (res == ULONG_MAX && errno) {
		fprintf(stderr, "strtoul %s: %s\n", nptr, strerror(errno));
		exit(1);
	}

	if (end && *end == nptr) {
		fprintf(stderr, "expected number, not \"%s\"\n", nptr);
		exit(1);
	}
	return res;
}

static unsigned int xstrtoui(const char *nptr, int base)
{
	char *end;
	unsigned long res;

	res = str_to_ulong(nptr, &end, base);

#if ULONG_MAX > UINT_MAX
	if (res > UINT_MAX) {
		fprintf(stderr, "%s: value cannot exceed %u\n", nptr, UINT_MAX);
		exit(1);
	}
#endif
	return (unsigned int) res;
}

static struct timespec parse_timer_delay(const char *str_const)
{
	char *end;
	struct timespec delay;
	uint64_t tmp;
	double timeval;
	static const double ns_per_s = 1000000000.0;

	errno = 0;
	timeval = strtod(str_const, &end);
	if (errno) {
		fprintf(stderr, "-d: %s: %s", str_const, strerror(errno));
		exit(1);
	}

	if (end == str_const) {
		fprintf(stderr, "-d: %s: not a number?", str_const);
		exit(1);
	}

	delay.tv_sec = (uint64_t) timeval;

	if (delay.tv_sec >= 1.0)
		timeval -= (double) (delay.tv_sec);

	if (timeval < ns_per_s) {
		timeval *= ns_per_s;
		tmp = (uint64_t) timeval;
	} else {
		tmp = 0;
		delay.tv_sec++;
	}
	delay.tv_nsec = tmp;

	return delay;
}

static void parse_u32_with_mask(const char *str_const, uint32_t *m, uint32_t *v)
{
	const char *ptr = strchr(str_const, '/');
	char *end;

	*v = (uint32_t) (ptr ? xstrtoui(ptr+1, 16) : ~0u);
	*m = (uint32_t) str_to_ulong(str_const, &end, 16);
	if (*end && *end != '/') {
		fprintf(stderr, "mark mask must be separated by '/', not '%c'\n", *end);
		exit(1);
	}
}

static void parse_set_ctmarkmask(const char *str_const)
{
	uint32_t mark, mask;

	parse_u32_with_mask(str_const, &mark, &mask);

	ctt_opts.ct_event_filter.ctmark = mark;
	ctt_opts.ct_event_filter.ctmask = mask;
}


static void options_pre(void)
{
	ctt_opts.delay.tv_sec = 2;
	ctt_opts.ctmark_group_mask = ~0u;

	cttop_options = &ctt_opts;
}

static void options_post(void)
{

	if (ctt_opts.batch_mode) {
		unsigned int i, group, count;

		for (i=0; i < GROUP_MAX &&
			!ctt_opts.batch_group_display[i]; i++)
			;
		group = i;

		for (count = 0, i=0; i < GROUP_MAX ; i++)
			if (ctt_opts.batch_group_display[i])
				count++;

		switch (count) {
		case 0: break;
		case 1:
			ctt_opts.batch_group_display[group] = true;
			ct_entry_toggle_group(group);
			break;
		default:
			ctt_opts.batch_group_display_cycle = true;
		}
	}
}

int cttop_parse_options(int argc, char *argv[])
{
	int val;
	enum group_type group;

	options_pre();

	while ((val = getopt_long(argc, argv, optstring, ctt_longopts, NULL)) != -1) {
		switch (val) {
		case 'b':
			ctt_opts.batch_mode = true;
			break;
		case 'd':
			ctt_opts.delay = parse_timer_delay(optarg);
			break;
		case 'g':
			group = groupname_to_enum(optarg);
			if (ctt_opts.batch_mode)
				ctt_opts.batch_group_display[group] = true;
			else
				ct_entry_toggle_group(group);
			break;
		case 'E':
			ctt_opts.max_lines = xstrtoui(optarg, 0);
			break;
		case 'F':
			ctt_opts.ctmark_force = true;
			break;

		case 'h':
			usage(stdout);
			exit(0);
		case 'L':
			ctt_opts.ct_event_filter.loopback = true;
			break;
		case 'n':
			ctt_opts.iterations = xstrtoui(optarg, 0);
			break;
		case 'm':
			parse_set_ctmarkmask(optarg);
			break;
		case 'M':
			ctt_opts.ctmark_group_mask = (uint32_t)
					str_to_ulong(optarg, NULL, 16);
			if (ctt_opts.ctmark_group_mask == 0) {
				fputs("group mask cannot be 0\n", stderr);
				exit(1);
			}
			break;
		case '?':
			usage(stderr);
			exit(1);
		default:
			fprintf(stderr, "Unknown option: \'%c\'\n", val);
			usage(stderr);
			exit(1);
		}
	}

	options_post();

	return 0;
}

