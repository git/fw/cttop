#include <stdbool.h>
#include <glib.h>

#include <libnetfilter_conntrack/libnetfilter_conntrack.h>

struct ct_acct {
	unsigned int flows;
	GHashTable *src_addrs;
};

static inline unsigned int ct_acct_get_flow_count(const struct ct_acct *acct)
{
	return acct->flows;
}

static inline unsigned int ct_acct_get_saddr_count(const struct ct_acct *acct)
{
	return g_hash_table_size(acct->src_addrs);
}

/*
 * remove connection ct from internal accounting stats, if any.
 * returns true if ct was the last connection accounted
 */
bool ct_acct_del(const struct nf_conntrack *ct, struct ct_acct *acct);

/*
 * add connection to internal accounting stats.
 */
void ct_acct_add(const struct nf_conntrack *ct, struct ct_acct *acct);

/*
 * initialize acct structure and add ct as first member.
 *
 * flags is used to control what properties should be accounted.
 *
 * flags 0: only track number of individual flows.
 *
 * Additional properties that can be accounted:
 * CTT_GRP_FLOWATTR_SRCCOUNT: account number of source addresses.
 * CTT_GRP_FLOWATTR_SRC: account number of source addresses.
 */
void ct_acct_init(const struct nf_conntrack *ct, struct ct_acct *acct, unsigned int flags);
