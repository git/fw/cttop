/*
 * This file contains an adapted version of lookup3.c, by Bob Jenkins
 * <bob_jenkins@burtleburtle.net>, from
 *   http://www.burtleburtle.net/bob/hash/index.html#lookup
 *
 * Thanks to Mr. Jenkins to put his work into the public domain. Likewise,
 * this file remains public domain as well. That is, it may be used by
 * anyone for any purpose, with or without fee, and there is no warranty
 * of any kind under any circumstance.
 *
 * -- Moritz Grimm <mgrimm@mrsserver.net>
 */

#include <sys/types.h>
#include <stdint.h>

#define GOLDEN_RATIO		 0x9ca57eadU

#define UHASH_SIZE(n)		((uint32_t)1 << (n))
#define UHASH_MASK(n)		(UHASH_SIZE(n) - 1)
#define UHASH_ROT(x, k) 	( ((x) << (k)) | ((x) >> (32 - (k))) )
/* UHASH_MIX() modifies its arguments. */
#define UHASH_MIX(a, b, c)					\
	{							\
		a -= c;  a ^= UHASH_ROT(c,  4);  c += b;	\
		b -= a;  b ^= UHASH_ROT(a,  6);  a += c;	\
		c -= b;  c ^= UHASH_ROT(b,  8);  b += a;	\
		a -= c;  a ^= UHASH_ROT(c, 16);  c += b;	\
		b -= a;  b ^= UHASH_ROT(a, 19);  a += c;	\
		c -= b;  c ^= UHASH_ROT(b,  4);  b += a;	\
	}
/* UHASH_FINAL() modifies its arguments. */
#define UHASH_FINAL(a, b, c)					\
	{							\
		c ^= b;  c -= UHASH_ROT(b, 14); 		\
		a ^= c;  a -= UHASH_ROT(c, 11); 		\
		b ^= a;  b -= UHASH_ROT(a, 25); 		\
		c ^= b;  c -= UHASH_ROT(b, 16); 		\
		a ^= c;  a -= UHASH_ROT(c, 4);			\
		b ^= a;  b -= UHASH_ROT(a, 14); 		\
		c ^= b;  c -= UHASH_ROT(b, 24); 		\
	}

static uint32_t
uhash_words(const uint32_t *keys, size_t num_keys, uint32_t initval)
{
	uint32_t	a, b, c;

	a = b = c = GOLDEN_RATIO + (((uint32_t)num_keys) << 2) + initval;

	while (num_keys > 3) {
		a += keys[0];
		b += keys[1];
		c += keys[2];
		UHASH_MIX(a, b, c);
		num_keys -= 3;
		keys += 3;
	}

	switch(num_keys) {
	case 3:
		c += keys[2];
	case 2:
		b += keys[1];
	case 1:
		a += keys[0];
		UHASH_FINAL(a, b, c);
	case 0:
	default:
		break;
	}

	return (c);
}

