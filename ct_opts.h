/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#include <stdbool.h>
#include <stdint.h>
#include <time.h>

#include "ct_entry.h"

struct ctt_opts {
	struct timespec delay;	/* screen refresh timer period */
	unsigned int iterations;
	unsigned int max_lines;
	bool batch_mode;
	struct {
		uint32_t ctmark;
		uint32_t ctmask;
		bool loopback;
	} ct_event_filter;
	uint32_t ctmark_group_mask;
	bool batch_group_display_cycle;
	bool batch_group_display[GROUP_MAX];
	bool ctmark_force;
};

extern const struct ctt_opts *cttop_options;

int cttop_parse_options(int argc, char *argv[]);
