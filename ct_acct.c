/*
 * (C) 2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <inttypes.h>

#include "cttop.h"
#include "ct_acct.h"
#include "ct_entry.h"
#include "jhash3.h"

struct ct_acct_addr {
	uint8_t family;
	uint32_t address[4];
	uint32_t count;
};

static void convert_nfct_to_acct_saddr(const struct nf_conntrack *ct, struct ct_acct_addr *addr)
{

	if (nfct_attr_is_set(ct, ATTR_IPV6_SRC)) {
		memcpy(addr->address, nfct_get_attr(ct, ATTR_IPV6_SRC),
					sizeof(addr->address));
		addr->family = AF_INET6;
		return;
	}
	addr->address[0] = nfct_get_attr_u32(ct, ATTR_IPV4_SRC);
	addr->family = AF_INET;
}

static guint ct_acct_hash(gconstpointer arg)
{
	const struct ct_acct_addr *a = arg;

	if (a->family == AF_INET)
		return uhash_words(a->address, 1, 0);

	return uhash_words(a->address, ARRAY_SIZE(a->address), 0);
}

static gboolean ct_acct_equal(gconstpointer a, gconstpointer b)
{
	const struct ct_acct_addr *a1 = a;
	const struct ct_acct_addr *a2 = b;

	if (a1->address[0] != a2->address[0])
		return false;
	if (a1->family != a2->family)
		return false;
	if (a1->family == AF_INET)
		return true;

	if ((a1->address[3] != a2->address[3]) ||
	    (a1->address[2] != a2->address[2]))
		return false;

	return a1->address[1] == a2->address[1];
}

static void acct_del_src(const struct nf_conntrack *ct, struct ct_acct *acct)
{
	struct ct_acct_addr key;
	uint32_t *count;

	if (!acct->src_addrs)
		return;

	convert_nfct_to_acct_saddr(ct, &key);

	count = g_hash_table_lookup(acct->src_addrs, &key);
	if (!count) {
		fprintf(stderr, "%s: internal error: tried to remove non-existant ct\n",
									__func__);
		return;
	}

	--*count;
	if (*count == 0)
		g_hash_table_remove(acct->src_addrs, &key);
	if (acct->flows == 0)
		g_hash_table_destroy(acct->src_addrs);
}

bool ct_acct_del(const struct nf_conntrack *ct, struct ct_acct *acct)
{
	if (acct->flows)
		acct->flows--;

	acct_del_src(ct, acct);

	return acct->flows == 0;
}

static void acct_add_src(const struct nf_conntrack *ct, struct ct_acct *acct)
{
	struct ct_acct_addr key;
	struct ct_acct_addr *addr;
	uint32_t *count;

	if (!acct->src_addrs)
		return;

	convert_nfct_to_acct_saddr(ct, &key);
	count = g_hash_table_lookup(acct->src_addrs, &key);
	if (count) {
		++*count;
		return;
	}

	addr = malloc(sizeof(*addr));
	if (!addr) {
		perror("malloc");
		exit(1);
	}
	memcpy(addr, &key, sizeof(*addr));
	addr->count = 1;
	g_hash_table_insert(acct->src_addrs, addr, &addr->count);
}

void ct_acct_add(const struct nf_conntrack *ct, struct ct_acct *acct)
{
	acct->flows++;
	acct_add_src(ct, acct);
}

void ct_acct_init(const struct nf_conntrack *ct,
		struct ct_acct *acct, unsigned int flags)
{
	acct->src_addrs = NULL;
	acct->flows = 0;

	if (flags & CTT_GRP_FLOWATTR_SRCCOUNT) {
		acct->src_addrs = g_hash_table_new_full(
				ct_acct_hash, ct_acct_equal,
				free, NULL);

		if (!acct->src_addrs) {
			perror("g_hash_table_new_full");
			exit(1);
		}
	}
	ct_acct_add(ct, acct);
}

