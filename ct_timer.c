/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include "ui.h"
#include "ct_opts.h"
#include "ct_entry.h"
#include "ct_timer.h"

static int timerfd;
static unsigned int iterations;
static struct timespec timer_interval;

static uint64_t tick_counter;

uint64_t cttimer_get_ticks(void)
{
	return tick_counter;
}

void cttimer_set_interval(const struct timespec *it_interval)
{
	struct itimerspec ts;

	ts.it_interval = *it_interval;
	ts.it_value = *it_interval;

	if (timerfd_settime(timerfd, 0, &ts, NULL) == 0)
		timer_interval = *it_interval;
}

struct timespec cttimer_get_interval(void)
{
	return timer_interval;
}

static void display_output(void)
{
	unsigned int i;

	if (!cttop_options->batch_group_display_cycle) {
		ui_redraw();
		return;
	}

	for (i = 0; i < GROUP_MAX ; i++) {
		if (cttop_options->batch_group_display[i]) {
			ct_entry_toggle_group(i);
			ui_redraw();
		}
	}
}

void cttimer_dispatch(void)
{
	uint64_t ticks;
	ssize_t ret;

	ret = read(timerfd, &ticks, sizeof(ticks));
	if (ret < 0) {
		perror("read timerfd");
		exit(1);
	}

	tick_counter += ticks;

	display_output();

	ct_entry_poll_counters();

	if (iterations == 0)
		return;

	iterations--;
	if (iterations == 0) {
		raise(SIGTERM);
		close(timerfd);
	}
}

double cttimer_get_ticks_per_sec(void)
{
	uint64_t ns_per_tick = timer_interval.tv_sec * NSEC_PER_SEC;
	ns_per_tick += timer_interval.tv_nsec;
	return ((double)NSEC_PER_SEC) / (double) ns_per_tick;
}

int cttimer_init(void)
{
	struct itimerspec ts = {
		.it_value = { .tv_nsec = NSEC_PER_SEC / 2UL },
	};

	ts.it_interval = cttop_options->delay;
	timer_interval = cttop_options->delay;

	timerfd = timerfd_create(CLOCK_MONOTONIC, TFD_CLOEXEC);
	if (timerfd < 0) {
		perror("timerfd_create");
		exit(1);
	}

	if (timerfd_settime(timerfd, 0, &ts, NULL)) {
		perror("timerfd_settime");
		exit(1);
	}

	iterations = cttop_options->iterations;

	return timerfd;
}
