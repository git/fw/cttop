/*
 * (C) 2010-2011 Florian Westphal <fw@strlen.de>
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 */
#ifndef CTTOP_UI_HDR_
#define CTTOP_UI_HDR_
#include <stdbool.h>

enum field {
	FIELD_L4PROTO,
	FIELD_SADDR,
	FIELD_DADDR,
	FIELD_INB,
	FIELD_OUTB,
	FIELD_INPKT,
	FIELD_OUTPKT,

	FIELD_AVG_INB,
	FIELD_AVG_OUTB,

	FIELD_SPORT,
	FIELD_DPORT,

	FIELD_CTMARK,
	FIELD_CTSECMARK,

	FIELD_FLOWS,
	FIELD_SRCCOUNT,

	__FIELD_MAX,
};
const char *field_to_shortname_str(enum field f);
const char *field_to_longname_str(enum field f);
char field_to_toggle_key(enum field f);
bool field_is_enabled(enum field f);
void field_toggle(enum field f);

struct ui_ops {
	void (*ui_help)(void);
	void (*ui_fields)(void);
	void (*ui_prompt_finish)(void);
	int (*ui_get_character)(void);
	int (*ui_print_fmt)(const char *fmt, ...);
	int (*ui_print_change_fmt)(const char *fmt, ...);
	int (*ui_print_prompt_fmt)(const char *fmt, ...);
	void (*ui_winch)(void);
	void (*ui_refresh)(void);
	unsigned int (*ui_get_linecount)(void);
	unsigned int (*ui_get_rowcount)(void);
};

void ui_dispatch(void);

void ui_winch(void);
void ui_redraw(void);

void ui_init(void);
void ui_exit(void);

#endif
